@extends('layouts.login')
@section('content')
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->


    <div id="login-page" class="row">
        @include('layouts.error')
        <div class=" col-md-3 card-panel col-md-push-4 ">
            <div>
                <form class="form form-validate floating-label" method="POST" action="{{ url('/login') }}"  novalidate="novalidate">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="input-field col s12 center">
                            <img src="{{asset('images/login-logo.png')}}" alt="" class="circle responsive-img valign profile-image-login">
                            <p class="center login-form-text" style="color: #05ad98"><b>Paintings</b>LOGIN</p>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="email" class="form-control" id="username" required name="email">
                                <label for="username">Username</label>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password" required name="password">
                                <label for="password">Password</label>
                                
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-sm-8">
                                    <button class="btn btn-primary btn-raised" type="submit">Login</button>
                                </div><!--end .col -->
                            </div><!--end .row -->
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <br><br>
        <div class="col-sm-3 col-sm-offset-5 text-center">
            <br><br><br><br><br><br>
                --or--
            </h5>
            <p>
               <a class="btn btn-block btn-raised btn-primary" href="{{ url('register') }}">Sign up here</a>
            </p>
        
        </div>
    </div>

@endsection

