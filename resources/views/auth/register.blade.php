@extends('layouts.register')
@section('content')

    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>

    <div class="spacer"></div>
    <div class="card contain-sm style-transparent">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <br/><br><br><br><br><br><br>
                    <span class="text-lg text-bold text-primary">Register here</span>
                    @include('layouts.error')
                    <form class="form form-validate floating-label" method="POST" action="{{ url('/register') }}"  novalidate="novalidate">
                        {{ csrf_field() }}
                        <br/>
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" required name="name">
                            <label for="name">Name</label>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" required name="email" value="{{ old('email') }}">
                            <label for="email">Email</label>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" required name="password">
                            <label for="password">Password</label>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="confirm_password" required name="password_confirmation">
                            <label for="confirm_password">Confirm Password</label>
                        </div>


                        <div class="row">
                            <div class="col-xs-6 text-left">
                                <div class="form-group">
                                    <div class="checkbox checkbox-styled">
                                        <label>
                                            <input type="checkbox" name="terms1" required>
                                            <span>Terms of service</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="col-xs-6 text-right">
                                <button class="btn btn-primary btn-raised" type="submit">Register</button>
                            </div><!--end .col -->
                        </div><!--end .row -->
                    </form>
                </div><!--end .col -->
                <br><br>
                <div class="col-sm-5 col-sm-offset-1 text-center">
                    <br><br>
                    <h5 class="text-light">
                        Already Have an Account?
                    </h5>
                    <a class="btn btn-block btn-raised btn-primary" href="{{ url('login') }}">Login Here</a>
                    <br><br>
                    
                </div><!--end .col -->
            </div><!--end .row -->
        </div><!--end .card-body -->
    </div><!--end .card -->
@endsection
