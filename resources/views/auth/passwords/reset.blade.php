
<!DOCTYPE HTML>
<html>
<head>
    <title>Stylish Decor-by preeti thaker arora</title>
    <link href="{{asset('front/css/bootstrap.css')}}" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary JavaScript plugins) -->
    <script type='text/javascript' src="{{asset('front/js/jquery-1.11.1.min.js')}}"></script>
    <!-- Custom Theme files -->
    <link href="{{asset('front/css/style.css')}}" rel='stylesheet' type='text/css' />
    <!-- Custom Theme files -->
    <!--//theme-style-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Furnyish Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href='http://fonts.googleapis.com/css?family=Montserrat|Raleway:400,200,300,500,600,700,800,900,100' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Aladin' rel='stylesheet' type='text/css'>
    <!-- start menu -->
    <link href="{{asset('front/css/megamenu.css')}}" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="{{asset('front/js/megamenu.js')}}"></script>
    <script src="{{asset('front/js/materialize.min.js')}}"></script>
    <script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
    <script src="{{asset('front/js/menu_jquery.js')}}"></script>
    <script src="{{asset('front/js/simpleCart.min.js')}}"> </script>
    <link href="{{asset('front/css/form.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('front/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="all">
    <link href="{{asset('front/css/materialize.min.css')}}" rel="stylesheet" type="text/css" >


</head>
<body>
<!-- header -->
<div class="top_bg">
    <div class="container">
        <div class="header_top-sec">
            <div class="top_right">
                <ul>
                    <li><a href="#">help</a></li>|
                    <li><a href="#contact">Contact</a></li>|
                </ul>
            </div>
            <div class="top_left">
                <ul>
                    <li>Email : <a class="mail" href="https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession">thepeoplesplace1476@yahoo.com</a></li>|
                </ul>
                <div class="social">
                    <ul>
                        <li><a href="https://www.facebook.com/preetithakerarora/"><i class="facebook"></i></a></li>
                        <li><a href="https://twitter.com/ThakerArora?s=08"><i class="twitter"></i></a></li>
                        <li><a href="#"><i class="dribble"></i></a></li>
                        <li><a href="#"><i class="google"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="header_top">
    <div class="container">
        <div class="logo">
            <a href="{{ url('/') }}"><img src="{{asset('front/images/logo.png')}}" alt=""/></a>
        </div>
        <div class="header_right">
            <div class="cart box_1">
                @if(isset($_SESSION['product']))
                    <a href="{{ url('cart') }}">
                        <h3> <span class="">{{ count($_SESSION['product']) }} items</span> <img src="{{asset('front/images/bag2.png')}}" alt=""></h3>
                    </a>
                @endif
                {{--<p><a href="javascript:;" class="simpleCart_empty">Empty cart</a></p>--}}
                <div class="clearfix"> </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
</div>
<!--cart-->

<!------>
<div class="mega_nav">
    <div class="container">
        <div class="menu_sec">
            <!-- start header menu -->
            <ul class="megamenu skyblue">
                <li class="active grid"><a class="color1" href="{{ url('/') }}">Home</a></li>
                <li class=" grid"><a class="color1" href="{{ url('about-us') }}">About Us</a></li>
                <li class=" grid"><a class="color1" href="{{ url('media') }}">Media</a></li>
                <li class=" grid"><a class="color1" href="{{ url('/') }}">Portfolio</a></li>
                <li class=" grid"><a class="color1" href="{{ url('/') }}">Corporate Gifting</a></li>
                <li class=" grid"><a class="color1" href="{{ url('/') }}">Charity</a></li>
                <li class=" grid"><a class="color1" href="{{ url('contact') }}">Contact us</a></li>
            </ul>



            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!---->
<!--header//-->
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{{--/header--}}
<div class="w3l-footer" id="contact">
    <div class="container">
        <div class="footer-info-agile">

            <div class="col-md-4 footer-info-grid address">
                <h4>ADDRESS</h4>
                <address>
                    <ul>
                        <li>2a c wing Crystal plaza,</li>
                        <li>Opp. Infinity mall </li>
                        <li>Andheri link Road,</li>
                        <li>Andheri west,
                            Mumbai 53</li>
                        <li>Telephone : +91 9920969994</li>
                        <li>Email : <a class="mail" href="https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession">thepeoplesplace1476@yahoo.com</a></li>
                    </ul>
                </address>
            </div>
            <div class="col-md-4 footer-info-grid links">
                <h4>QUICK LINKS</h4>
                <ul>
                    <li><a  href="{{ url('about-us') }}">About Us</a></li>
                    <li><a  href="{{ url('talk_town') }}">Talk of the town</a></li>
                    <li><a  href="{{ url('signature') }}">Signature collection</a></li>
                    <li><a  href="{{ url('elite') }}">Elite Cosmic collection</a><li>
                    <li><a  href="#contact">Contact</a><li>
                </ul>
            </div>

            <div class="col-md-4 footer-grid newsletter connect-social">
                <h4>Get in Touch</h4>
                <section class="social1">
                    <ul>
                        <li><a class="icon fb" href="https://www.facebook.com/preetithakerarora/"><i class="fa fa-facebook"></i></a></li>
                        <li><a class="icon tw" href="https://twitter.com/ThakerArora?s=08"><i class="fa fa-twitter"></i></a></li>
                        <li><a class="icon db" href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li><a class="icon gp" href="#"><i class="fa fa-google-plus"></i></a></li>


                    </ul>
                </section>

                <div class="clearfix"> </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="copyright-wthree">
            <p>&copy; 2017 STYLISH DECOR. All Rights Reserved | Design by <a href="http://www.letscodeprivatelimited.com/"> LETS CODE PRIVATE LIMITED</a></p>
        </div>

    </div>
</div>

<!---->
</body>
</html>