
<!DOCTYPE HTML>
<html>
<head>
    <title>Stylish Decor-by preeti thaker arora</title>
    <link href="{{asset('front/css/bootstrap.css')}}" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary JavaScript plugins) -->

    <!-- Custom Theme files -->
    <link href="{{asset('front/css/style.css')}}" rel='stylesheet' type='text/css' />
    <!-- Custom Theme files -->
    <!--//theme-style-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" />
    <script type='text/javascript' src="{{asset('front/js/jquery-1.11.1.min.js')}}"></script>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href='http://fonts.googleapis.com/css?family=Montserrat|Raleway:400,200,300,500,600,700,800,900,100' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Aladin' rel='stylesheet' type='text/css'>
    <!-- start menu -->
    <link href="{{asset('front/css/megamenu.css')}}" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="{{asset('front/js/megamenu.js')}}"></script>
    <script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
    <script src="{{asset('front/js/menu_jquery.js')}}"></script>
    <script src="{{asset('front/js/simpleCart.min.js')}}"> </script>
    <link href="{{asset('front/css/form.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('front/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="all">

    <script src="{{asset('front/js/bootstrap.js')}}"></script>
</head>
<body>
<!-- header -->
<div class="top_bg">
    <div class="container-fluid">
        <div class="header_top-sec">
            <div class="top_right">
                <ul>
                    <li><a href="#">help</a></li>|
                    <li><a href="#contact">Contact</a></li>|
                </ul>
            </div>
            <div class="top_left">
                <div class="col-md-5">
                    <ul>
                        <li>Email : <a class="mail" href="https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession">thepeoplesplace1476@yahoo.com</a></li>|
                    </ul>
                </div>
                <div class="social col-md-4 col-xs-6">
                    <ul>
                        <li><a href="https://www.facebook.com/preetithakerarora/"><i class="facebook"></i></a></li>
                        <li><a href="https://twitter.com/ThakerArora?s=08"><i class="twitter"></i></a></li>
                        <li><a href="#"><i class="dribble"></i></a></li>
                        <li><a href="#"><i class="google"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-6">
                    @if(\Illuminate\Support\Facades\Auth::check())
                        <ul>
                            <li class="dropdown tag one">
                                <a href="#" class="dropdown-toggle" id="drop1" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-tag"></span> {{ \Illuminate\Support\Facades\Auth::user()->name}}</a>
                                <ul class="dropdown-menu" aria-labelledby="drop1">
                                    <li><a href="{{url('myaccount')}}">My Account</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{url('myorder')}}">My Order</a></li>
                                </ul>
                            </li>
                            <li class="tag">
                                <a  href="{{ url('/logout') }}"><span class="glyphicon glyphicon-log-in"></span>Logout</a>
                            </li>
                        </ul>
                    @else
                        <ul>
                            <li class="tag one"><a href="#" data-toggle="modal" data-target="#myModal11"><span class="glyphicon glyphicon-tag"></span> Login</a></li>
                            <li class="tag"><a href="#" data-toggle="modal" data-target="#myModal12"><span class="glyphicon glyphicon-log-in"></span> Sign Up</a></li>
                        </ul>
                    @endif
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal11" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-info">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body modal-spa">
                <div class="login-grids">
                    <div class="login-bottom">
                        <h3>Login</h3>
                        <form action="{{ url('/login') }}" method="post">
                            {{ csrf_field() }}
                            <input type="text" class="user" name="email" placeholder="Email" required="">
                            <input type="password" name="password" placeholder="Password" required="">
                            <input type="submit" value="Submit">
                            &nbsp;
                            <a href="#" data-toggle="modal" data-target="#myModal12">Create A New Account?</a><br><br>
                            <a href="{{url('forget')}}">Forgot Password</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //login -->
<!--/signup -->
<div class="modal fade" id="myModal12" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-info">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body modal-spa">
                <div class="login-grids">
                    <div class="login-bottom">
                        <h3>Sign Up</h3>
                        <form action="{{ url('/register') }}" method="post">
                            {{ csrf_field() }}
                            <input type="text" name="name" placeholder="Your Name" required="">
                            <input type="email" name="email" placeholder="Your Email" required="">
                            <input type="tel" name="mobile" placeholder="Mobile" required="">
                            <input type="password" name="password" placeholder="Password" required="">
                            <input type="password" name="password_confirmation" placeholder="Confirm Password" required="">
                            <div class="signup">
												<span class="agree-checkbox">
													<label class="checkbox"><input type="checkbox" name="checkbox">I agree to your <a class="w3layouts-t" href="#">Terms of Use</a></label>
												</span>
                            </div>
                            <div class="clearfix"></div>
                            <input type="submit" value="Sign Up">
                            &nbsp;<a href="#" data-toggle="modal" data-target="#myModal11">Already A Member?</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //signup -->
<div class="header_top">
    <div class="container">
        <div class="logo">
            <a href="{{ url('/') }}"><img src="{{asset('front/images/logo.png')}}" alt=""/></a>
        </div>
        <div class="header_right">
            <div class="cart box_1">
                @if(isset($_SESSION['product']))
                    <a href="{{ url('cart') }}">
                        <h3> <span class="">{{ count($_SESSION['product']) }} items</span> <img src="{{asset('front/images/bag2.png')}}" alt=""></h3>
                    </a>
                @endif
                {{--<p><a href="javascript:;" class="simpleCart_empty">Empty cart</a></p>--}}
                <div class="clearfix"> </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
</div>
<!--cart-->

<!------>
<div class="mega_nav">
    <div class="container">
        <div class="menu_sec">
            <!-- start header menu -->
            <ul class="megamenu skyblue">
                <li class="active grid"><a class="color1" href="{{ url('/') }}">Home</a></li>
                <li class=" grid"><a class="color1" href="{{ url('about-us') }}">About Us</a></li>
                <li class=" grid"><a class="color1" href="{{ url('media') }}">Media</a></li>
                <li class=" grid"><a class="color1" href="{{ url('/') }}">Portfolio</a></li>
                <li class=" grid"><a class="color1" href="{{ url('/') }}">Corporate Gifting</a></li>
                <li class=" grid"><a class="color1" href="{{url('charity')}}">Charity</a></li>
                <li class=" grid"><a class="color1" href="{{ url('contact') }}">Contact us</a></li>
            </ul>



            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!---->
<!--header//-->
<div class="container">
    <div class="col-md-12" style="padding: 0;">
        <div class="col-md-8 niche1" style="padding: 0;">
            <p style="font-size: 20px;    margin: 50px 0;
	color: #6d6d6d;">  India's leading Artist and an Entrepreneur Preeti Thaker Arora  creates breath-takingly beautiful paintings and has put them altogether in a very Stylish way to Embellish the Beauty of your walls to create a Luxurious, Comfortable, Cozy and Fashionable Ambience.

                <br> <br> The mastermind Preeti Thaker Arora   matches her elegance into creating beautiful ambiances using her unique and crisp stlyles of paintings which changes the entre look of your walls and surroundings.

                <br><br>  Preeti strongly believes that the purpose of our lives is to be useful, to be compassionate, to have made some difference in othrers lives.

                <br><br> Hence she is the first and the only artist encouraging and promoting rural artists in India. Her paintings are a true blend of class and quality which adds colour and happiness in people’s lives. She donates 50% of her income to Orphanages, physically impaired, abandoned children, Cancer patients.

                <br> <br> She Received the “Most Outstanding Woman for Social Work” 2017 conducted by “FEMINA” in conjunction with World HRD Congress.

                <br> <br>Her mesmerizing paintings are beyond imagination which brings about peace and happiness around your premises.
                May it be a close friend's or relative's wedding or a casual evening party her paintings are ideal for gifting to people whom you love and care about.

                <br><br>Preeti belives that its a myth that paintings are only meant for the rich,  wealthy and elite. She wants to break that stereotype belife that persists in people’s minds hence her range of paintings are very reasonable and easily affordable to anyone who likes buy them.

            </p>
        </div>
        <div class="col-md-4 upper1" style="margin: 20px 0;">
            <img src="{{ asset('front/images/20170325_101230.jpg') }}" class="img-responsive" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
            <h4 style="margin-top: 20px; font-weight: bold"> Preeti Thaker Arora</h4>
        </div>
    </div></div>


<div class="w3l-footer" id="contact">
    <div class="container">
        <div class="footer-info-agile">

            <div class="col-md-4 footer-info-grid address">
                <h4>ADDRESS</h4>
                <address>
                    <ul>
                        <li>2a c wing Crystal plaza,</li>
                        <li>Opp. Infinity mall </li>
                        <li>Andheri link Road,</li>
                        <li>Andheri west,
                            Mumbai 53</li>
                        <li>Telephone : +91 9920969994</li>
                        <li>Email : <a class="mail" href="https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession">thepeoplesplace1476@yahoo.com</a></li>
                    </ul>
                </address>
            </div>
            <div class="col-md-4 footer-info-grid links">
                <h4>QUICK LINKS</h4>
                <ul>
                    <li><a  href="{{ url('about-us') }}">About Us</a></li>
                    <li><a  href="{{ url('talk_town') }}">Talk of the town</a></li>
                    <li><a  href="{{ url('signature') }}">Signature collection</a></li>
                    <li><a  href="{{ url('elite') }}">Elite Cosmic collection</a><li>
                    <li><a  href="#contact">Contact</a><li>
                </ul>
            </div>

            <div class="col-md-4 footer-grid newsletter connect-social">
                <h4>Get in Touch</h4>
                <section class="social1">
                    <ul>
                        <li><a class="icon fb" href="https://www.facebook.com/preetithakerarora/"><i class="fa fa-facebook"></i></a></li>
                        <li><a class="icon tw" href="https://twitter.com/ThakerArora?s=08"><i class="fa fa-twitter"></i></a></li>
                        <li><a class="icon db" href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li><a class="icon gp" href="#"><i class="fa fa-google-plus"></i></a></li>


                    </ul>
                </section>

                <div class="clearfix"> </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="copyright-wthree">
            <p>&copy; 2017 STYLISH DECOR. All Rights Reserved | Design by <a href="http://www.letscodeprivatelimited.com/"> LETS CODE PRIVATE LIMITED</a></p>
        </div>

    </div>
</div>

<!---->
</body>
</html>