
<!DOCTYPE HTML>
<html>
<head>
	<title>Mumbai</title>
	<link href="{{asset('front/css/bootstrap.css')}}" rel='stylesheet' type='text/css' />
	<!-- jQuery (necessary JavaScript plugins) -->
	<script type='text/javascript' src="{{asset('front/js/jquery-1.11.1.min.js')}}"></script>
	<!-- Custom Theme files -->
	<link href="{{asset('front/css/style.css')}}" rel='stylesheet' type='text/css' />
	<!-- Custom Theme files -->
	<!--//theme-style-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords"  />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<link href='http://fonts.googleapis.com/css?family=Montserrat|Raleway:400,200,300,500,600,700,800,900,100' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Aladin' rel='stylesheet' type='text/css'>
	<!-- start menu -->
	<link href="{{asset('front/css/megamenu.css')}}" rel="stylesheet" type="text/css" media="all" />

	<link href="{{asset('front/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="all">
	<script type="text/javascript" src="{{asset('front/js/megamenu.js')}}"></script>
	<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
	<script src="{{asset('front/js/menu_jquery.js')}}"></script>
	<script src="{{asset('front/js/simpleCart.min.js')}}"> </script>
	<script src="{{asset('front/js/responsiveslides.min.js')}}"></script>
	<script src="{{asset('front/js/bootstrap.js')}}"></script>
	<script>
		// You can also use "$(window).load(function() {"
		$(function () {
			// Slideshow 1
			$("#slider1").responsiveSlides({
				auto: true,
				nav: true,
				speed: 500,
				namespace: "callbacks",
			});
		});
	</script>

</head>
<body>
<div class="top_bg">
	<div class="container-fluid">
		<div class="header_top-sec">
			<div class="top_right">
				<ul>
					<li><a href="#">help</a></li>|
					<li><a href="#contact">Contact</a></li>|
				</ul>
			</div>
			<div class="top_left">
				<div class="col-md-5">
					<ul>
						<li>Email : <a class="mail" href="https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession">thepeoplesplace1476@yahoo.com</a></li>|
					</ul>
				</div>
				<div class="social col-md-4 col-xs-6">
					<ul>
						<li><a href="https://www.facebook.com/preetithakerarora/"><i class="facebook"></i></a></li>
						<li><a href="https://twitter.com/ThakerArora?s=08"><i class="twitter"></i></a></li>
						<li><a href="#"><i class="dribble"></i></a></li>
						<li><a href="#"><i class="google"></i></a></li>
					</ul>
				</div>
				<div class="col-md-3 col-xs-6">
					@if(\Illuminate\Support\Facades\Auth::check())
						<ul>
							<li class="dropdown tag one">
								<a href="#" class="dropdown-toggle" id="drop1" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-tag"></span> {{ \Illuminate\Support\Facades\Auth::user()->name}}</a>
								<ul class="dropdown-menu" aria-labelledby="drop1">
									<li><a href="{{url('myaccount')}}">My Account</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="{{url('myorder')}}">My Order</a></li>
								</ul>
							</li>
							<li class="tag">
								<a  href="{{ url('/logout') }}"><span class="glyphicon glyphicon-log-in"></span>Logout</a>
							</li>
						</ul>
					@else
						<ul>
							<li class="tag one"><a href="#" data-toggle="modal" data-target="#myModal11"><span class="glyphicon glyphicon-tag"></span> Login</a></li>
							<li class="tag"><a href="#" data-toggle="modal" data-target="#myModal12"><span class="glyphicon glyphicon-log-in"></span> Sign Up</a></li>
						</ul>
					@endif
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<div class="modal fade" id="myModal11" tabindex="-1" role="dialog" >
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-info">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body modal-spa">
				<div class="login-grids">
					<div class="login-bottom">
						<h3>Login</h3>
						<form action="{{ url('/login') }}" method="post">
							{{ csrf_field() }}
							<input type="text" class="user" name="email" placeholder="Email" required="">
							<input type="password" name="password" placeholder="Password" required="">
							<input type="submit" value="Submit">
							&nbsp;
							<a href="#" data-toggle="modal" data-target="#myModal12">Create A New Account?</a><br><br>
							<a href="{{url('forget')}}">Forgot Password</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //login -->
<!--/signup -->
<div class="modal fade" id="myModal12" tabindex="-1" role="dialog" >
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-info">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body modal-spa">
				<div class="login-grids">
					<div class="login-bottom">
						<h3>Sign Up</h3>
						<form action="{{ url('/register') }}" method="post">
							{{ csrf_field() }}
							<input type="text" name="name" placeholder="Your Name" required="">
							<input type="email" name="email" placeholder="Your Email" required="">
							<input type="tel" name="mobile" placeholder="Mobile" required="">
							<input type="password" name="password" placeholder="Password" required="">
							<input type="password" name="password_confirmation" placeholder="Confirm Password" required="">
							<div class="signup">
												<span class="agree-checkbox">
													<label class="checkbox"><input type="checkbox" name="checkbox">I agree to your <a class="w3layouts-t" href="#">Terms of Use</a></label>
												</span>
							</div>
							<div class="clearfix"></div>
							<input type="submit" value="Sign Up">
							&nbsp;<a href="#" data-toggle="modal" data-target="#myModal11">Already A Member?</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="header_top">
	<div class="container">
		<div class="logo">
			<a href="{{ url('/') }}"><img src="{{asset('front/images/logo.png')}}" alt=""/></a>
		</div>
		@php
			$counter=Auth::user()->cart->count();
			$counter == 0 ? $counts=0 : $counts=$counter;
			//$counts=Auth::user()->cart->count()-1;
		@endphp
		@if($counts>0)
		@if(\Illuminate\Support\Facades\Auth::check())
		<div class="header_right">
			<div class="cart box_1">
					<a href="{{ url('cart') }}">
						<h3>  {{ $counts }} items <img src="{{asset('front/images/bag2.png')}}" alt=""></h3>
					</a>
				{{--<p><a href="javascript:;" class="simpleCart_empty">Empty cart</a></p>--}}
				<div class="clearfix"> </div>
			</div>
		</div>
			@else
				<div class="header_right">
					<div class="cart box_1">
						<a href="#" data-toggle="modal" data-target="#myModal11" class="btn9"><h3> <span class="simpleCart_total">0 items</span> <img src="{{asset('front/images/bag2.png')}}" alt=""></h3></a>
						{{--<a href="{{ url('cart') }}">--}}
						{{--<h3> <span class="simpleCart_total" style="color: #fff">0 items</span> <img src="{{asset('front/images/bag.png')}}" alt=""></h3>--}}
						{{--</a>--}}
						{{--<p><a href="javascript:;" class="simpleCart_empty">Empty cart</a></p>--}}
						<div class="clearfix"> </div>
					</div>
		</div>
		@endif
		@else

		@endif

		<div class="clearfix"></div>
	</div>
</div>
<!--cart-->

<!------>
<div class="mega_nav">
	<div class="container">
		<div class="menu_sec">
			<!-- start header menu -->
			<ul class="megamenu skyblue">
				<li class="active grid"><a class="color1" href="{{ url('/') }}">Home</a></li>
				<li class=" grid"><a class="color1" href="{{ url('about-us') }}">About Us</a></li>
				<li class=" grid"><a class="color1" href="{{ url('media') }}">Media</a></li>
				<li class=" grid"><a class="color1" href="{{ url('/') }}">Portfolio</a></li>
				<li class=" grid"><a class="color1" href="{{ url('/') }}">Corporate Gifting</a></li>
				<li class=" grid"><a class="color1" href="{{url('charity')}}">Charity</a></li>
				<li class=" grid"><a class="color1" href="{{ url('contact') }}">Contact us</a></li>
			</ul>



			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!---->
<!--header//-->
<div class="cart_main">
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="{{ url('/') }}">Home</a></li>
			<li class="active">Cart</li>
		</ol>


		{{--code to show cart items starts here--}}
		<div class="cart-items">
			@if($counts > 0)
			<?php
				$total = '0';
			?>
			<h2>My Shopping Bag ({{ $counts }} Items)</h2>
			@if($counts>0)
				@if(count($cart)>0)
				@foreach($cart as $datas)
				<script>$(document).ready(function(c) {
						$('.datas{{$datas->id}}').on('click', function(c){
							$.ajax({url: "{{ url('delete-cart',$datas->cartid) }}", success: function(result){
								location.reload();
							}});
						});
					});
				</script>

				<div class="cart-header">
					<div class="close1 datas{{$datas->id}}"> </div>
					<div class="cart-sec">
						<div class="cart-item cyc">
                            <img src="{{ asset('app/'.$datas->image ?? '')  }}"/>
						</div>
						<div class="cart-item-info">
							<h3>{{ $datas->name }}</h3>
							<h4><span>Rs. </span>{{ $datas->amount ?? 5000 }}</h4>
							<p class="qty">Size :: {{ $datas->height }} X {{ $datas->width }}</p>

							{{--<input min="1" type="number" id="quantity" name="quantity" value="1" class="form-control input-small">--}}
						</div>
						<div class="clearfix"></div>
						<div class="delivery">
							{{--<p>Service Charges:: Rs.50.00</p>--}}
						</div>
						<?php
						$total+=$datas->amount;
						$_SESSION['total']=$total;
						?>
					</div>
				</div>
				@endforeach
			@endif
			@endif
			@else
				<h2>Oops! Seems like you have an empty cart. <a href="{{ url('/') }}">Lets add some items in it.</a> </h2>
			@endif
		</div>
{{--code to show cart items ends here--}}

		@if($counts >0)
		<div class="cart-total">
			<div class="price-details">
				<h3>Price Details</h3>
				<span>Total</span>
				<span class="total">{{ $total }}</span>
				<span>Discount</span>
				<span class="total">---</span>
				<span>Delivery Charges</span>
				<span class="total">300.00</span>
				<div class="clearfix"></div>
			</div>
			<h4 class="last-price">TOTAL</h4>
			<span class="total final">{{ $total+300 }}</span>
			<div class="clearfix"></div>
			<a class="order" href="{{url('paymentoption')}}">Place Order</a>
			<div class="total-item">
			</div>
		</div>
		@endif
	</div>
</div>
<!---->


<div class="w3l-footer" id="contact">
	<div class="container">
		<div class="footer-info-agile">

			<div class="col-md-4 footer-info-grid address">
				<h4>ADDRESS</h4>
				<address>
					<ul>
						<li>2a c wing Crystal plaza,</li>
						<li>Opp. Infinity mall </li>
						<li>Andheri link Road,</li>
						<li>Andheri west,
							Mumbai 53</li>
						<li>Telephone : +91 9920969994</li>
						<li>Email : <a class="mail" href="https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession">thepeoplesplace1476@yahoo.com</a></li>
					</ul>
				</address>
			</div>
			<div class="col-md-4 footer-info-grid links">
				<h4>QUICK LINKS</h4>
				<ul>
					<li><a  href="{{ url('about-us') }}">About Us</a></li>
					<li><a  href="{{ url('talk_town') }}">Talk of the town</a></li>
					<li><a  href="{{ url('signature') }}">Signature collection</a></li>
					<li><a  href="{{ url('elite') }}">Elite Cosmic collection</a><li>
					<li><a  href="#contact">Contact</a><li>
				</ul>
			</div>

			<div class="col-md-4 footer-grid newsletter connect-social">
				<h4>Get in Touch</h4>
				<section class="social1">
					<ul>
						<li><a class="icon fb" href="https://www.facebook.com/preetithakerarora/"><i class="fa fa-facebook"></i></a></li>
						<li><a class="icon tw" href="https://twitter.com/ThakerArora?s=08"><i class="fa fa-twitter"></i></a></li>
						<li><a class="icon db" href="#"><i class="fa fa-dribbble"></i></a></li>
						<li><a class="icon gp" href="#"><i class="fa fa-google-plus"></i></a></li>


					</ul>
				</section>

				<div class="clearfix"> </div>
			</div>

			<div class="clearfix"></div>
		</div>

		<div class="copyright-wthree">
			<p>&copy; 2017 STYLISH DECOR. All Rights Reserved | Design by <a href="http://www.letscodeprivatelimited.com/"> LETS CODE PRIVATE LIMITED</a></p>
		</div>

	</div>
</div>
<!---->
</body>
</html>
