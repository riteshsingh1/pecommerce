
<!DOCTYPE HTML>
<html>
<head>
	<title>Stylish Decor-by preeti thaker arora</title>
	<link href="{{asset('front/css/bootstrap.css')}}" rel='stylesheet' type='text/css' />
	<!-- jQuery (necessary JavaScript plugins) -->
	<script type='text/javascript' src="{{asset('front/js/jquery-1.11.1.min.js')}}"></script>
	<!-- Custom Theme files -->
	<link href="{{asset('front/css/style.css')}}" rel='stylesheet' type='text/css' />
	<!-- Custom Theme files -->
	<!--//theme-style-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords"  />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<link href='https://fonts.googleapis.com/css?family=Montserrat|Raleway:400,200,300,500,600,700,800,900,100' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Aladin' rel='stylesheet' type='text/css'>
	<!-- start menu -->
	<link href="{{asset('front/css/megamenu.css')}}" rel="stylesheet" type="text/css" media="all" />

	<link href="{{asset('front/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="all">
	<script type="text/javascript" src="{{asset('front/js/megamenu.js')}}"></script>
	<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
	<script src="{{asset('front/js/menu_jquery.js')}}"></script>
	<script src="{{asset('front/js/bootstrap.js')}}"></script>
	<script src="{{asset('front/js/simpleCart.min.js')}}"> </script>

	<link href="{{asset('front/css/form.css')}}" rel="stylesheet" type="text/css" media="all" />
	<!--etalage-->
	<link rel="stylesheet" href="{{asset('front/css/etalage.css')}}">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>
	@if(session('status'))
		<script>
            $(document).ready(function () {
                swal(
                    'Congratulations !',
                    '{{ session('status') }}',
                    'success'
                )
            });
		</script>
	@endif
	<script src="{{asset('front/js/jquery.etalage.min.js')}}"></script>
	<script>
		jQuery(document).ready(function($){

			$('#etalage').etalage({
				thumb_image_width: 300,
				thumb_image_height: 400,
//				source_image_width: 900,
//				source_image_height: 1200,
				show_hint: true,
				click_callback: function(image_anchor, instance_id){
					alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
				}
			});

		});


	</script>


	<!-- //etalage-->

</head>
<body>
<!-- header -->
<div class="top_bg">
	<div class="container-fluid">
		<div class="header_top-sec">
			<div class="top_right">
				<ul>
					<li><a href="#">help</a></li>|
					<li><a href="#contact">Contact</a></li>|
				</ul>
			</div>
			<div class="top_left">
				<div class="col-md-5">
					<ul>
						<li>Email : <a class="mail" href="https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession">thepeoplesplace1476@yahoo.com</a></li>|
					</ul>
				</div>
				<div class="social col-md-4 col-xs-6">
					<ul>
						<li><a href="https://www.facebook.com/preetithakerarora/"><i class="facebook"></i></a></li>
						<li><a href="https://twitter.com/ThakerArora?s=08"><i class="twitter"></i></a></li>
						<li><a href="#"><i class="dribble"></i></a></li>
						<li><a href="#"><i class="google"></i></a></li>
					</ul>
				</div>
				<div class="col-md-3 col-xs-6">
					@if(\Illuminate\Support\Facades\Auth::check())
						<ul>
							<li class="dropdown tag one">
								<a href="#" class="dropdown-toggle" id="drop1" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-tag"></span> {{ \Illuminate\Support\Facades\Auth::user()->name}}</a>
								<ul class="dropdown-menu" aria-labelledby="drop1">
									<li><a href="{{url('myaccount')}}">My Account</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="{{url('myorder')}}">My Order</a></li>
								</ul>
							</li>
							<li class="tag">
								<a  href="{{ url('/logout') }}"><span class="glyphicon glyphicon-log-in"></span>Logout</a>
							</li>
						</ul>
					@else
						<ul>
							<li class="tag one"><a href="#" data-toggle="modal" data-target="#myModal11"><span class="glyphicon glyphicon-tag"></span> Login</a></li>
							<li class="tag"><a href="#" data-toggle="modal" data-target="#myModal12"><span class="glyphicon glyphicon-log-in"></span> Sign Up</a></li>
						</ul>
					@endif
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<div class="modal fade" id="myModal11" tabindex="-1" role="dialog" >
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-info">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body modal-spa">
				<div class="login-grids">
					<div class="login-bottom">
						<h3>Login</h3>
						<form action="{{ url('/login') }}" method="post">
							{{ csrf_field() }}
							<input type="text" class="user" name="email" placeholder="Email" required="">
							<input type="password" name="password" placeholder="Password" required="">
							<input type="submit" value="Submit">
							&nbsp;
							<a href="#" data-toggle="modal" data-target="#myModal12">Create A New Account?</a><br><br>
							<a href="{{url('forget')}}">Forgot Password</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //login -->
<!--/signup -->
<div class="modal fade" id="myModal12" tabindex="-1" role="dialog" >
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-info">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body modal-spa">
				<div class="login-grids">
					<div class="login-bottom">
						<h3>Sign Up</h3>
						<form action="{{ url('/register') }}" method="post">
							{{ csrf_field() }}
							<input type="text" name="name" placeholder="Your Name" required="">
							<input type="email" name="email" placeholder="Your Email" required="">
							<input type="tel" name="mobile" placeholder="Mobile" required="">
							<input type="password" name="password" placeholder="Password" required="">
							<input type="password" name="password_confirmation" placeholder="Confirm Password" required="">
							<div class="signup">
												<span class="agree-checkbox">
													<label class="checkbox"><input type="checkbox" name="checkbox">I agree to your <a class="w3layouts-t" href="#">Terms of Use</a></label>
												</span>
							</div>
							<div class="clearfix"></div>
							<input type="submit" value="Sign Up">
							&nbsp;<a href="#" data-toggle="modal" data-target="#myModal11">Already A Member?</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="header_top">
	<div class="container">
		<div class="logo">
			<a href="{{ url('/') }}"><img src="{{asset('front/images/logo.png')}}" alt=""/></a>
		</div>
		@if(\Illuminate\Support\Facades\Auth::check())
			@php
				$counter=Auth::user()->cart->count();
                            $counter == 0 ? $counts=0 : $counts=$counter;
			@endphp
			<div class="header_right">
				<div class="cart box_1">
					<a href="{{ url('cart') }}">
						<h3> {{ $counts }} items <img src="{{asset('front/images/bag2.png')}}" alt=""></h3>
					</a>
					{{--<p><a href="javascript:;" class="simpleCart_empty">Empty cart</a></p>--}}
					<div class="clearfix"> </div>
				</div>
			</div>
		@else
			<div class="header_right">
				<div class="cart box_1">
					<a href="#" data-toggle="modal" data-target="#myModal11" class="btn9"><h3> <span class="simpleCart_total" style="color: #fff">0 items</span> <img src="{{asset('front/images/bag2.png')}}" alt=""></h3></a>
					{{--<a href="{{ url('cart') }}">--}}
					{{--<h3> <span class="simpleCart_total" style="color: #fff">0 items</span> <img src="{{asset('front/images/bag.png')}}" alt=""></h3>--}}
					{{--</a>--}}
					{{--<p><a href="javascript:;" class="simpleCart_empty">Empty cart</a></p>--}}
					<div class="clearfix"> </div>
				</div>
			</div>
		@endif
		<div class="clearfix"></div>
	</div>
</div>
<!--cart-->

<!------>
<div class="mega_nav">
	<div class="container">
		<div class="menu_sec">
			<!-- start header menu -->
			<ul class="megamenu skyblue">
				<li class="active grid"><a class="color1" href="{{ url('/') }}">Home</a></li>
				<li class=" grid"><a class="color1" href="{{ url('about-us') }}">About Us</a></li>
				<li class=" grid"><a class="color1" href="{{ url('media') }}">Media</a></li>
				<li class=" grid"><a class="color1" href="{{ url('/') }}">Portfolio</a></li>
				<li class=" grid"><a class="color1" href="{{ url('/') }}">Corporate Gifting</a></li>
				<li class=" grid"><a class="color1" href="{{url('charity')}}">Charity</a></li>
				<li class=" grid"><a class="color1" href="{{ url('contact') }}">Contact us</a></li>
			</ul>



			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!---->
<div class="single-sec">
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="{{ url('/') }}">Home</a></li>
			<li class="active">{{ $product->name}}</li>
		</ol>
		<!-- start content -->
		<div class="col-md-12 det">
			<div class="single_left" style="height: auto !important;">
				<div class="grid images_3_of_2" style="height: auto !important;">
					<ul id="etalage" class="etalage" style="height: auto !important;" >
						<li>
							<a href="#">
								<img class="etalage_thumb_image  img-responsive" src="{{ asset('app/'.$product->image) }}"  style="height: auto !important;"/>
								<img class="etalage_source_image img-responsive" src="{{ asset('app/'.$product->image) }}" style="height: auto !important;" title="" />
							</a>
						</li>

					</ul>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="single-right">
				<h3>{{ $product->name }}</h3>
				<h5 class="qty">Size :: {{ $product->height }} X {{ $product->width }}</h5>
				<!-- <div class="id"><h4>ID: SB2379</h4></div> -->
				<form action="" class="sky-form">
					<fieldset>
						<section>
							<div class="rating">
								<input type="radio" name="stars-rating" id="stars-rating-5">
								<label for="stars-rating-5"><i class="icon-star"></i></label>
								<input type="radio" name="stars-rating" id="stars-rating-4">
								<label for="stars-rating-4"><i class="icon-star"></i></label>
								<input type="radio" name="stars-rating" id="stars-rating-3">
								<label for="stars-rating-3"><i class="icon-star"></i></label>
								<input type="radio" name="stars-rating" id="stars-rating-2">
								<label for="stars-rating-2"><i class="icon-star"></i></label>
								<input type="radio" name="stars-rating" id="stars-rating-1">
								<label for="stars-rating-1"><i class="icon-star"></i></label>
								<div class="clearfix"></div>
							</div>
						</section>
					</fieldset>
				</form>
				<div class="cost">
					<div class="prdt-cost">
						<ul>
							<h4><a href="{{ url('single',$product->id) }}">{{ $product->name }}</a></h4><br>
							<p class="qty">Size :: {{ $product->height }} X {{ $product->width }}</p><br>
							<h4>Rs:
								@php
									$price = $product->detail->amount;
                                    $amount = ($price/6)*10;

								@endphp
								<strike>{{ $amount }}</strike>
								<br>Discount: 40% off</h4>
							<h4 style="color:red;font-size: 20px;margin-top: 10px;">Selling Price: Rs{{ $product->detail->amount }}</h4><br>

							{{--<li>Shipping Charges: Rs 300/- extra</li>--}}
							@if(\Illuminate\Support\Facades\Auth::guest())

								<div class="tag one"><a href="#" data-toggle="modal" data-target="#myModal11"><span class="glyphicon glyphicon-tag"></span> Buy Now</a></div>
								<div class="tag one"><a href="#" data-toggle="modal" data-target="#myModal11"><span class="glyphicon glyphicon-tag"></span> Add to cart</a></div>

							@else
							{{--<form action="{{ url('proceed-checkout') }}" method="post">--}}
								{{--{{ csrf_field() }}--}}
								{{--<input type="hidden" name="amount" value="{{ $product->detail->amount }}">--}}
								{{--<button type="submit" class="btn9" name="submit" value="Proceed">Buy Now</button>--}}
							{{--</form>--}}
								<a href="{{ url('cart',$product->id) }}" class="btn9">Add to Cart</a>
							@endif

							</ul>
					</div>
					{{--<div class="check">--}}
						{{--<p><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>Enter pin code for delivery & availability</p>--}}
						{{--<form class="navbar-form navbar-left" role="search">--}}
							{{--<div class="form-group">--}}
								{{--<input type="text" class="form-control" placeholder="Enter Pin code">--}}
							{{--</div>--}}
							{{--<button type="submit" class="btn  btn-default btn5">Verify</button>--}}
						{{--</form>--}}
					{{--</div>--}}
					<div class="clearfix"></div>
				</div>

				<div class="single-bottom1">
					<h6>Description</h6>
					<p class="prod-desc">{{ $product->description }}</p>
				</div>
				<div class="single-bottom1">
					<h6>Specifications</h6>
					<div class="item-list">
						<ul>
							<li><b>Delivery : </b>{{ $product->spec->Delivery ?? '' }}</li>
							<li><b>Quality : </b>{{ $product->spec->Quality ?? ''}}</li>
							<li><b>Durability : </b>{{ $product->spec->Durability ?? ''}}</li>
							<li><b>Return Policy : </b>{{ $product->spec->Return_Policy ?? ''}}</li>
							<li><b>Medium : </b>{{ $product->spec->Medium ?? ''}}</li>
							<li><b>Cancellation : </b>{{ $product->spec->Cancellation ?? ''}}</li>
						</ul>
					</div>
				</div>
			</div>

	<div class="clearfix"></div>
</div>
</div>
<!---->
	<br><br>

	<div class="w3l-footer" id="contact">
		<div class="container">
			<div class="footer-info-agile">

				<div class="col-md-4 footer-info-grid address">
					<h4>ADDRESS</h4>
					<address>
						<ul>
							<li>2a c wing Crystal plaza,</li>
							<li>Opp. Infinity mall </li>
							<li>Andheri link Road,</li>
							<li>Andheri west,
								Mumbai 53</li>
							<li>Telephone : +91 9920969994</li>
							<li>Email : <a class="mail" href="https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession">thepeoplesplace1476@yahoo.com</a></li>
						</ul>
					</address>
				</div>
				<div class="col-md-4 footer-info-grid links">
					<h4>QUICK LINKS</h4>
					<ul>
						<li><a  href="{{ url('about-us') }}">About Us</a></li>
						<li><a  href="{{ url('talk_town') }}">Talk of the town</a></li>
						<li><a  href="{{ url('signature') }}">Signature collection</a></li>

						<li><a  href="{{ url('elite') }}">Elite Cosmic collection</a><li>
						<li><a  href="#contact">Contact</a><li>
					</ul>
				</div>

				<div class="col-md-4 footer-grid newsletter connect-social">
					<h4>Get in Touch</h4>
					<section class="social1">
						<ul>
							<li><a class="icon fb" href="https://www.facebook.com/preetithakerarora/"><i class="fa fa-facebook"></i></a></li>
							<li><a class="icon tw" href="https://twitter.com/ThakerArora?s=08"><i class="fa fa-twitter"></i></a></li>
							<li><a class="icon db" href="#"><i class="fa fa-dribbble"></i></a></li>
							<li><a class="icon gp" href="#"><i class="fa fa-google-plus"></i></a></li>


						</ul>
					</section>

					<div class="clearfix"> </div>
				</div>

				<div class="clearfix"></div>
			</div>

			<div class="copyright-wthree">
				<p>&copy; 2017 STYLISH DECOR. All Rights Reserved | Design by <a href="http://www.letscodeprivatelimited.com/"> LETS CODE PRIVATE LIMITED</a></p>
			</div>

		</div>
	</div>
</div>

<!---->
</body>
</html>