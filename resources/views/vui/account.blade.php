
<!DOCTYPE HTML>
<html>
<head>
	<title>mumbai</title>
	<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
	<!-- jQuery (necessary JavaScript plugins) -->
	<script type='text/javascript' src="js/jquery-1.11.1.min.js"></script>
	<!-- Custom Theme files -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<!-- Custom Theme files -->
	<!--//theme-style-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Furnyish Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<link href='http://fonts.googleapis.com/css?family=Montserrat|Raleway:400,200,300,500,600,700,800,900,100' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Aladin' rel='stylesheet' type='text/css'>
	<!-- start menu -->
	<link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
	<script type="text/javascript" src="js/megamenu.js"></script>
	<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
	<script src="js/menu_jquery.js"></script>
	<script src="js/simpleCart.min.js"> </script>

</head>
<body><!-- header -->
<div class="top_bg">
	<div class="container">
		<div class="header_top-sec">
			<div class="top_right">
				<ul>
					<li><a href="#">help</a></li>|
					<li><a href="#contact">Contact</a></li>|
				</ul>
			</div>
			<div class="top_left">
				<ul>
					<li class="top_link">Email: <a href="mailto@example.com">info(at)Mobilya.com</a></li>|
					<li class="top_link"><a href="login.html">Pt76mumbai@gmail.com</a></li>|
				</ul>
				<div class="social">
					<ul>
						<li><a href="https://www.facebook.com/preetithakerarora/"><i class="facebook"></i></a></li>
						<li><a href="https://twitter.com/ThakerArora?s=08"><i class="twitter"></i></a></li>
						<li><a href="#"><i class="dribble"></i></a></li>
						<li><a href="#"><i class="google"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<div class="header_top">
	<div class="container">
		<div class="logo">
			<a href="index.html"><img src="{{asset('front/images/logo.png')}}" alt=""/></a>
		</div>
		{{--<div class="header_right">--}}
		{{--<div class="login">--}}
		{{--<a href="login.html">LOGIN</a>--}}
		{{--</div>--}}
		{{--<div class="cart box_1">--}}
		{{--<a href="cart.html">--}}
		{{--<h3> <span class="simpleCart_total">$0.00</span> (<span id="simpleCart_quantity" class="simpleCart_quantity">0</span> items)<img src="images/bag.png" alt=""></h3>--}}
		{{--</a>--}}
		{{--<p><a href="javascript:;" class="simpleCart_empty">Empty cart</a></p>--}}
		{{--<div class="clearfix"> </div>--}}
		{{--</div>--}}
		{{--</div>--}}
		<div class="clearfix"></div>
	</div>
</div>
<!--cart-->

<!------>
<div class="mega_nav">
	<div class="container">
		<div class="menu_sec">
			<!-- start header menu -->
			<ul class="megamenu skyblue">
				<li class="active grid"><a class="color1" href="{{ url('/') }}">Home</a></li>
				<li class=" grid"><a class="color1" href="{{ url('/') }}">About Us</a></li>
				<li class=" grid"><a class="color1" href="{{ url('/') }}">Media</a></li>
				<li class=" grid"><a class="color1" href="{{ url('/') }}">Portolio</a></li>
				<li class=" grid"><a class="color1" href="{{ url('/') }}">Coporate Gifting</a></li>
				<li class=" grid"><a class="color1" href="{{ url('/') }}">Charity</a></li>
				<li class=" grid"><a class="color1" href="#contact">Contact us</a></li>
			</ul>



			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!---->
<!--header//-->
<div class="container">
	<ol class="breadcrumb">
		<li><a href="index.html">Home</a></li>
		<li class="active">Account</li>
	</ol>
	<div class="registration">
		<div class="registration_left">
			<h2>new user? <span> create an account </span></h2>
			<!-- [if IE]
               < link rel='stylesheet' type='text/css' href='ie.css'/>
            [endif] -->

			<!-- [if lt IE 7]>
               < link rel='stylesheet' type='text/css' href='ie6.css'/>
            <! [endif] -->
			<script>
				(function() {

					// Create input element for testing
					var inputs = document.createElement('input');

					// Create the supports object
					var supports = {};

					supports.autofocus   = 'autofocus' in inputs;
					supports.required    = 'required' in inputs;
					supports.placeholder = 'placeholder' in inputs;

					// Fallback for autofocus attribute
					if(!supports.autofocus) {

					}

					// Fallback for required attribute
					if(!supports.required) {

					}

					// Fallback for placeholder attribute
					if(!supports.placeholder) {

					}

					// Change text inside send button on submit
					var send = document.getElementById('register-submit');
					if(send) {
						send.onclick = function () {
							this.innerHTML = '...Sending';
						}
					}

				})();
			</script>
			<div class="registration_form">
				<!-- Form -->
				<form id="registration_form" action="contact.php" method="post">
					<div>
						<label>
							<input placeholder="first name:" type="text" tabindex="1" required autofocus>
						</label>
					</div>
					<div>
						<label>
							<input placeholder="last name:" type="text" tabindex="2" required autofocus>
						</label>
					</div>
					<div>
						<label>
							<input placeholder="email address:" type="email" tabindex="3" required>
						</label>
					</div>
					<div>
						<label>
							<input placeholder="Mobile:" type="email" tabindex="3" required>
						</label>
					</div>
					<div class="sky_form1">
						<ul>
							<li><label class="radio left"><input type="radio" name="radio" checked=""><i></i>Male</label></li>
							<li><label class="radio"><input type="radio" name="radio"><i></i>Female</label></li>
							<div class="clearfix"></div>
						</ul>
					</div>
					<div>
						<label>
							<input placeholder="password" type="password" tabindex="4" required>
						</label>
					</div>
					<div>
						<label>
							<input placeholder="retype password" type="password" tabindex="4" required>
						</label>
					</div>
					<div>
						<input type="submit" value="create an account" id="register-submit">
					</div>
					<div class="sky-form">
						<label class="checkbox"><input type="checkbox" name="checkbox" ><i></i>i agree to mobilya.com &nbsp;<a class="terms" href="#"> terms of service</a> </label>
					</div>
				</form>
				<!-- /Form -->
			</div>
		</div>
		<div class="registration_left">
			<h2>existing user</h2>
			<div class="registration_form">
				<!-- Form -->
				<form id="registration_form" action="contact.php" method="post">
					<div>
						<label>
							<input placeholder="email:" type="email" tabindex="3" required>
						</label>
					</div>
					<div>
						<label>
							<input placeholder="password" type="password" tabindex="4" required>
						</label>
					</div>
					<div>
						<input type="submit" value="sign in" id="register-submit">
					</div>
					<div class="forget">
						<a href="#">forgot your password</a>
					</div>
				</form>
				<!-- /Form -->
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- end registration -->

<div class="w3l-footer" id="contact">
	<div class="container">
		<div class="footer-info-agile">

			<div class="col-md-4 footer-info-grid address">
				<h4>ADDRESS</h4>
				<address>
					<ul>
						<li>2a c wing Crystal plaza,</li>
						<li>Opp. Infinity mall </li>
						<li>Andheri link Road,</li>
						<li>Andheri west,
							Mumbai 53</li>
						<li>Telephone : +91 9920969994</li>
						<li>Email : <a class="mail" href="https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession">Pt76mumbai@gmail.com</a></li>
					</ul>
				</address>
			</div>
			<div class="col-md-4 footer-info-grid links">
				<h4>QUICK LINKS</h4>
				<ul>
					<li><a class="scroll" href="#about">About Us</a></li>
					<li><a class="scroll" href="{{ url('talk_town') }}">Talk of the town</a></li>
					<li><a class="scroll" href="{{ url('signature') }}">Signature collection</a></li>
					<li><a class="scroll" href="{{ url('luxury') }}">Luxury collection</a></li>
					<li><a class="scroll" href="{{ url('elite') }}">Elite Cosmic collection</a><li>
					<li><a class="scroll" href="#contact">Contact</a><li>
				</ul>
			</div>

			<div class="col-md-4 footer-grid newsletter connect-social">
				<h4>Get in Touch</h4>
				<section class="social1">
					<ul>
						<li><a class="icon fb" href="https://www.facebook.com/preetithakerarora/"><i class="fa fa-facebook"></i></a></li>
						<li><a class="icon tw" href="https://twitter.com/ThakerArora?s=08"><i class="fa fa-twitter"></i></a></li>
						<li><a class="icon db" href="#"><i class="fa fa-dribbble"></i></a></li>
						<li><a class="icon gp" href="#"><i class="fa fa-google-plus"></i></a></li>


					</ul>
				</section>

				<div class="clearfix"> </div>
			</div>

			<div class="clearfix"></div>
		</div>

		<div class="copyright-wthree">
			<p>&copy; 2017 SYLISH DECOR. All Rights Reserved | Design by <a href="http://www.letscodeprivatelimited.com/"> LETS CODE PRIVATE LIMITED</a></p>
		</div>

	</div>
</div>
<!---->
</body>
</html>


