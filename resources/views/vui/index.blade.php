
<!DOCTYPE html>
<html>
<head>
<title>Stylish Decor-by preeti thaker arora</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"  />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="{{asset('front/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
<link href="{{asset('front/css/style2.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{asset('front/css/ninja-slider.css')}}" rel="stylesheet" type="text/css" media="all"/>

<link href="{{asset('front/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" type="text/css" href="{{asset('front/css/component.css')}}"/>
<link rel="stylesheet" href="{{asset('front/css/chocolat.css')}}" type="text/css" >
<link href="{{asset('front/css/index.css')}}" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="{{asset('front/css/owl.carousel.css')}}" type="text/css" media="all">
<link href="{{asset('front/css/iconeffects.css')}}" rel='stylesheet' type='text/css' />
<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.css" rel="stylesheet" type="text/css"/>
<!--fonts-->
<link href="https://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Ewert" rel="stylesheet">

<!--//fonts-->
<!--fonts-->
	<style>
		#snackbar {
			visibility: hidden;
			min-width: 250px;
			margin-left: -125px;
			background-color: #333;
			color: #fff;
			text-align: center;
			border-radius: 2px;
			padding: 16px;
			position: fixed;
			z-index: 1;
			left: 50%;
			bottom: 30px;
			font-size: 17px;
		}

		#snackbar.show {
			visibility: visible;
			-webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
			animation: fadein 0.5s, fadeout 0.5s 2.5s;
		}

		@-webkit-keyframes fadein {
			from {bottom: 0; opacity: 0;}
			to {bottom: 30px; opacity: 1;}
		}

		@keyframes fadein {
			from {bottom: 0; opacity: 0;}
			to {bottom: 30px; opacity: 1;}
		}

		@-webkit-keyframes fadeout {
			from {bottom: 30px; opacity: 1;}
			to {bottom: 0; opacity: 0;}
		}

		@keyframes fadeout {
			from {bottom: 30px; opacity: 1;}
			to {bottom: 0; opacity: 0;}
		}
	</style>
  

</head>
<!--Header-->
	<div class="header" id="home">
		<!-- /header-left -->
		          <div class="header-left">
				  <!-- /sidebar -->
				        <div id="sidebar"> 
						     <h4 class="menu">Stylish Decor</h4>

							<ul>
								<li><a class="" href="{{ url('about-us') }}">About Us</a></li>

								<li><a href="#">Categories <i class="glyphicon glyphicon-triangle-bottom"> </i> </a>
									<ul>

										<li><a class="" href="{{ url('talk_town') }}">Talk of the town</a></li>
										<li><a class="" href="{{ url('signature') }}">Signature collection</a></li>
										<li><a class="" href="{{ url('elite') }}">Elite Cosmic collection</a><li>

									</ul>
								</li>
								<li><a class="" href="{{url('media')}}">Media</a></li>
								<li><a class="" href="#about">Portfolio</a></li>
								<li><a class="" href="#about">Corporate Gifting</a></li>
								<li><a class="" href="{{url('charity')}}">Charity</a></li>
								<li><a class="" href="{{url('contact')}}">Contact Us</a></li>
							</ul>



						   <div id="sidebar-btn">
							   <span></span>
							   <span></span>
							   <span></span>
						   </div>
					   </div>

							 <script>
								  var sidebarbtn = document.getElementById('sidebar-btn');
									var sidebar = document.getElementById('sidebar');
								   sidebarbtn.addEventListener('click', function () {
								  if(sidebar.classList.contains('visible')){
										sidebar.classList.remove('visible'); 
								   }else {
										sidebar.className = 'visible';
									}
								  });
								</script>
					    <!-- //sidebar -->
					</div>
				  <!-- //header-left -->
				  <!-- /header-right -->
				    <div class="header-right">
					   @if(\Illuminate\Support\Facades\Auth::check())
							<ul>
								<li class="dropdown tag one">
									<a href="#" class="dropdown-toggle" id="drop1" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-tag"></span> {{ \Illuminate\Support\Facades\Auth::user()->name}}</a>
									<ul class="dropdown-menu" aria-labelledby="drop1">
										<li><a href="{{url('myaccount')}}">My Account</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="{{url('myorder')}}">My Order</a></li>
									</ul>
								</li>
								<li class="tag">
									<a  href="{{ url('/logout') }}"><span class="glyphicon glyphicon-log-in"></span>Logout</a>
								</li>
							</ul>



						   @else
							<div class="tag one"><a href="#" data-toggle="modal" data-target="#myModal11"><span class="glyphicon glyphicon-tag"></span> Login</a></div>
							<div class="tag"><a href="#" data-toggle="modal" data-target="#myModal12"><span class="glyphicon glyphicon-log-in"></span> Sign Up</a></div>
						@endif
					            <ul class="top-links">
									<li><a href="https://www.facebook.com/preetithakerarora/"><i class="fa fa-facebook"></i></a></li>
									<li><a href="https://twitter.com/ThakerArora?s=08"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>
					</div>
				  <!-- //header-right -->
				  <div class="clearfix"></div>
				  	<!-- login -->
			<div class="modal fade" id="myModal11" tabindex="-1" role="dialog" >
				<div class="modal-dialog" role="document">
					<div class="modal-content modal-info">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						</div>
						<div class="modal-body modal-spa">
							<div class="login-grids">
								<div class="login-bottom">
									<h3>Login</h3>
									<form action="{{ url('/login') }}" method="post">
											{{ csrf_field() }}
									         <input type="text" class="user" name="email" placeholder="Email" required>
											 <input type="password" name="password" placeholder="Password" required>
											 <input type="submit" value="Submit">
										&nbsp;
										<a href="#" data-toggle="modal" data-target="#myModal12">Create A New Account?</a><br><br>
										<a href="{{url('forget')}}">Forgot Password</a>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
<!-- //login -->
	<!--/signup -->
			<div class="modal fade" id="myModal12" tabindex="-1" role="dialog" >
				<div class="modal-dialog" role="document">
					<div class="modal-content modal-info">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						</div>
						<div class="modal-body modal-spa">
							<div class="login-grids">
								<div class="login-bottom">
									<h3>Sign Up</h3>
									<form action="{{ url('/register') }}" method="post">
											{{ csrf_field() }}
											<input type="text" name="name" placeholder="Your Name" required>
											<input type="email" name="email" placeholder="Your Email" required>
											<input type="tel" name="mobile" placeholder="Mobile" required>
											<input type="password" name="password" placeholder="Password" required>
											<input type="password" name="password_confirmation" placeholder="Confirm Password" required>
											<div class="signup">
												<span class="agree-checkbox">
													<label class="checkbox"><input type="checkbox" name="checkbox">I agree to your <a class="w3layouts-t" href="#">Terms of Use</a></label>
												</span>
											</div>
											<div class="clearfix"></div>
											<input type="submit" value="Sign Up">
										&nbsp;<a href="#" data-toggle="modal" data-target="#myModal11">Already A Member?</a>
										</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
<!-- //signup -->
		@if (session('status'))
			{{--Sucess Modal--}}
			<div class="modal fade" id="successmodal" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Stylish Decor</h4>
						</div>
						<div class="modal-body">
							<p>{{ session('status') }}</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>

					</div>
				</div>
			</div>
			{{--/Success Modal--}}
		@endif
		<div class="modal fade" id="errormodal" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Oops !</h4>
					</div>
					<div class="modal-body">
						<p>Please Check Your input. Invalid Credentials</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		{{--add to cart start here --}}
		@if(\Illuminate\Support\Facades\Auth::check())
			<div class="col-md-2 col-sm-4 col-xs-6  pull-right">
			<div class="cart box_1" style="top: 60px !important; right: 20px; position: absolute;z-index: 10000;">
				<a href="{{ url('cart') }}">
					@php
						$cart_count=Auth::user()->cart->count()-1;
						$counter=Auth::user()->cart->count();
						$counter == 0 ? $counts=0 : $counts=$counter;

					@endphp
					<h3> <span class="simpleCart_total" style="color: #fff"> {{ $counts }} items</span> <img src="{{asset('front/images/bag.png')}}" alt=""></h3>
				</a>
				{{--<p><a href="javascript:;" class="simpleCart_empty">Empty cart</a></p>--}}
				<div class="clearfix"> </div>
			</div>
			</div>
		@else
			<div class="col-md-2 col-sm-4 col-xs-6  pull-right">
				<div class="cart box_1" style="top: 40px !important; right: 20px; position: absolute;z-index: 10000;">
					<a href="#" data-toggle="modal" data-target="#myModal11" class="btn9"><h3> <span class="simpleCart_total" style="color: #fff">0 items</span> <img src="{{asset('front/images/bag.png')}}" alt=""></h3></a>
					{{--<a href="{{ url('cart') }}">--}}
						{{--<h3> <span class="simpleCart_total" style="color: #fff">0 items</span> <img src="{{asset('front/images/bag.png')}}" alt=""></h3>--}}
					{{--</a>--}}
					{{--<p><a href="javascript:;" class="simpleCart_empty">Empty cart</a></p>--}}
					<div class="clearfix"> </div>
				</div>
			</div>
		@endif
		{{--add to cart end here--}}
		<!--Slider-->
		<div class="slider">
			<div class="callbacks_container">
				<ul class="rslides" id="slider3">
					@foreach($main_slider as $main)
					<li>
						<div class="slider-img">
							<img src="{{ asset('app/'.$main->image) }}" class="img-responsive" alt="Homey Designs">
						</div>
						<div class="slider-info">
							 <h3 class="logo"><a href="#">Stylish Decor</a></h3>
							   <h4 class="color1"><i>{{ $main->name }}</i></h4>

						</div>
					</li>
					@endforeach

				</ul>
				
			</div>
			<div class="clearfix"></div>
		</div>
		<!--//Slider-->
	</div>
<div class="container-fluid" style="padding: 0px;">
<img src="front/images/discount.jpg" class="img-responsive">
</div>
	
	<!-- about -->

	      <div class="about" id="about">
		        <div class="container">

				         <div class="about-w3l-agileifo-grid">
				                  <div class="agile-w3l-ab">
								       <ul class="rslides" id="slider">
										@foreach($about_us as $about)
										<li>
											<div class="agile-w3l-ab-img">
												<img src="{{ asset('app/'.$about->image) }}" class="img-responsive" >
											</div>

										</li>

										@endforeach
									</ul>
				                  </div>


				          </div>

				        </div>

		  </div>
	<!-- //about -->

<div class="new">
	<div class="container">
		<h3>Make your living space come to life</h3>
		<div class="new-products">
			@foreach($showcase as $show)
				@if ($loop->iteration == '1' || $loop->iteration == '4' )
					<div class="new-items">
						@if ($loop->iteration == 1)
							<div class="item1">
								<a href="{{ url('single',$show->id) }}"><img src="{{ asset('app/'.$show->image) }}" alt=""/></a>
								<div class="item-info">
									<h4><a href="{{ url('single',$show->id) }}">{{ $show->name }}</a></h4><br>
									<p class="qty">Size :: {{ $show->height }} X {{ $show->width }}</p><br>
									<h4>Rs:
										@php
											$price = $show->detail->amount;
											$amount = ($price/6)*10;

										@endphp
										<strike>{{ $amount }}</strike>
										<br>Discount: 40% off</h4>
										<h4 style="color:red;font-size: 20px;margin-top: 10px;">Selling Price: Rs{{ $show->detail->amount }}</h4><br>

									@if($show->status)
										<a href="{{ url('single',$show->id) }}">Buy Now</a>
										@if(\Illuminate\Support\Facades\Auth::check())
											<a href="{{ url('cart',$show->id) }}" class="btn9">Add to Cart</a>
										@else
											<a href="#" data-toggle="modal" data-target="#myModal11" class="btn9">Add to Cart</a>
										@endif
									@else
										<a href="#" class="btn btn-info disabled btn22" type="button">Sold Out</a>
									@endif
								</div>
							</div>
					    @endif
						@if($loop->iteration == 4)
						<div class="item4">
							<a href="{{ url('single',$show->id) }}"><img src="{{ asset('app/'.$show->image) }}" alt=""/></a>
							<div class="item-info4">
								<h4><a href="{{ url('single',$show->id) }}">{{ $show->name }}</a></h4><br>
								<p class="qty">Size :: {{ $show->height }} X {{ $show->width }}</p><br>
								<h4>Rs:
									@php
										$price = $show->detail->amount;
                                        $amount = ($price/6)*10;

									@endphp
									<strike>{{ $amount }}</strike>
									<br>Discount: 40% off</h4>
								<h4 style="color:red;font-size: 20px;margin-top: 10px;">Selling Price: Rs{{ $show->detail->amount }}</h4><br>
		<br>
								@if($show->status==1)
									<a href="{{ url('single',$show->id) }}">Buy Now</a>
									@if(\Illuminate\Support\Facades\Auth::check())
										<a href="{{ url('cart',$show->id) }}" class="btn9">Add to Cart</a>
									@else
										<a href="#" data-toggle="modal" data-target="#myModal11" class="btn9">Add to Cart</a>
									@endif
								@else
									<a href="#" class="btn btn-info disabled btn22" type="button">Sold Out</a>
								@endif
							</div>
						</div>
						@endif
					</div>
			@endif
			@if ($loop->iteration == '2' || $loop->iteration == '5' )
			<div class="new-items new_middle">
				@if($loop->iteration == 2)
				<div class="item2">
					<a href="{{ url('single',$show->id) }}"><img src="{{ asset('app/'.$show->image) }}" alt=""/></a>
					<div class="item-info2">
						<h4><a href="{{ url('single',$show->id) }}">{{ $show->name }}</a></h4><br>
						<p class="qty">Size :: {{ $show->height }} X {{ $show->width }}</p><br>
						<h4>Rs:
							@php
								$price = $show->detail->amount;
                                $amount = ($price/6)*10;

							@endphp
							<strike>{{ $amount }}</strike>
							<br>Discount: 40% off</h4>
						<h4 style="color:red;font-size: 20px;margin-top: 10px;">Selling Price: Rs{{ $show->detail->amount }}</h4><br>
						@if($show->status==1)
							<a href="{{ url('single',$show->id) }}">Buy Now</a>
							@if(\Illuminate\Support\Facades\Auth::check())
								<a href="{{ url('cart',$show->id) }}" class="btn9">Add to Cart</a>
							@else
								<a href="#" data-toggle="modal" data-target="#myModal11" class="btn9">Add to Cart</a>
							@endif
						@else
							<a href="#" class="btn btn-info disabled btn22" type="button">Sold Out</a>
						@endif
					</div>
				</div>
				@endif
				@if($loop->iteration == 5)
				<div class="item5">
					<a href="{{ url('single',$show->id) }}"><img src="{{ asset('app/'.$show->image) }}" alt=""/></a>
					<div class="item-info5">
						<h4><a href="{{ url('single',$show->id) }}">{{ $show->name }}</a></h4><br>
						<p class="qty">Size :: {{ $show->height }} X {{ $show->width }}</p><br>
						<h4>Rs:
							@php
								$price = $show->detail->amount;
                                $amount = ($price/6)*10;

							@endphp
							<strike>{{ $amount }}</strike>
							<br>Discount: 40% off</h4>
						<h4 style="color:red;font-size: 20px;margin-top: 10px;">Selling Price: Rs{{ $show->detail->amount }}</h4><br>
						@if($show->status==1)
							<a href="{{ url('single',$show->id) }}">Buy Now</a>
							@if(\Illuminate\Support\Facades\Auth::check())
								<a href="{{ url('cart',$show->id) }}" class="btn9">Add to Cart</a>
							@else
								<a href="#" data-toggle="modal" data-target="#myModal11" class="btn9">Add to Cart</a>
							@endif
						@else
							<a href="#" class="btn btn-info disabled btn22" type="button">Sold Out</a>
						@endif
					</div>
				</div>
				@endif
			</div>
			@endif
			@if ($loop->iteration == '3' || $loop->iteration == '6' )
			<div class="new-items new_last">
				@if($loop->iteration == 3)
				<div class="item3">
					<a href="{{ url('single',$show->id) }}"><img src="{{ asset('app/'.$show->image) }}" alt=""/></a>
					<div class="item-info3">
						<h4><a href="{{ url('single',$show->id) }}">{{ $show->name }}</a></h4><br>
						<p class="qty">Size :: {{ $show->height }} X {{ $show->width }}</p><br>
						<h4>Rs:
							@php
								$price = $show->detail->amount;
                                $amount = ($price/6)*10;

							@endphp
							<strike>{{ $amount }}</strike>
							<br>Discount: 40% off</h4>
						<h4 style="color:red;font-size: 20px;margin-top: 10px;">Selling Price: Rs{{ $show->detail->amount }}</h4><br>
						@if($show->status==1)
							<a href="{{ url('single',$show->id) }}">Buy Now</a>
							@if(\Illuminate\Support\Facades\Auth::check())
								<a href="{{ url('cart',$show->id) }}" class="btn9">Add to Cart</a>
							@else
								<a href="#" data-toggle="modal" data-target="#myModal11" class="btn9">Add to Cart</a>
							@endif
						@else
						<a href="#" class="btn btn-info disabled btn22" type="button">Sold Out</a>
						@endif
					</div>
				</div>
				@endif
				@if($loop->iteration == 6)
				<div class="item6">
					<a href="{{ url('single',$show->id) }}"><img src="{{ asset('app/'.$show->image) }}" alt=""/></a>
					<div class="item-info6">
						<h4><a href="{{ url('single',$show->id) }}">{{ $show->name }}</a></h4><br>
						<p class="qty">Size :: {{ $show->height }} X {{ $show->width }}</p><br>
						<h4>Rs:
							@php
								$price = $show->detail->amount;
                                $amount = ($price/6)*10;

							@endphp
							<strike>{{ $amount }}</strike>
							<br>Discount: 40% off</h4>
						<h4 style="color:red;font-size: 20px;margin-top: 10px;">Selling Price: Rs{{ $show->detail->amount }}</h4><br>
						@if($show->status)
							<a href="{{ url('single',$show->id) }}">Buy Now</a>
							@if(\Illuminate\Support\Facades\Auth::check())
								<a href="{{ url('cart',$show->id) }}" class="btn9">Add to Cart</a>
							@else
								<a href="#" data-toggle="modal" data-target="#myModal11" class="btn9">Add to Cart</a>
							@endif
						@else
							<a href="#" class="btn btn-info disabled btn22" type="button">Sold Out</a>
						@endif
					</div>
				</div>
				@endif
			</div>
			@endif
					@if ($loop->iteration == '7' || $loop->iteration == '10' )
						<div class="new-items noone">
							@if ($loop->iteration == 7)
								<div class="item1">
									<a href="{{ url('single',$show->id) }}"><img src="{{ asset('app/'.$show->image) }}" alt=""/></a>
									<div class="item-info">
										<h4><a href="{{ url('single',$show->id) }}">{{ $show->name }}</a></h4><br>
										<p class="qty">Size :: {{ $show->height }} X {{ $show->width }}</p><br>
										<h4>Rs:
											@php
												$price = $show->detail->amount;
                                                $amount = ($price/6)*10;

											@endphp
											<strike>{{ $amount }}</strike>
											<br>Discount: 40% off</h4>
										<h4 style="color:red;font-size: 20px;margin-top: 10px;">Selling Price: Rs{{ $show->detail->amount }}</h4><br>

										@if($show->status)
											<a href="{{ url('single',$show->id) }}">Buy Now</a>
											@if(\Illuminate\Support\Facades\Auth::check())
												<a href="{{ url('cart',$show->id) }}" class="btn9">Add to Cart</a>
											@else
												<a href="#" data-toggle="modal" data-target="#myModal11" class="btn9">Add to Cart</a>
											@endif
										@else
											<a href="#" class="btn btn-info disabled btn22" type="button">Sold Out</a>
										@endif
									</div>
								</div>
							@endif
							@if($loop->iteration == 10)
								<div class="item4">
									<a href="{{ url('single',$show->id) }}"><img src="{{ asset('app/'.$show->image) }}" alt=""/></a>
									<div class="item-info4">
										<h4><a href="{{ url('single',$show->id) }}">{{ $show->name }}</a></h4><br>
										<p class="qty">Size :: {{ $show->height }} X {{ $show->width }}</p><br>
										<h4>Rs:
											@php
												$price = $show->detail->amount;
                                                $amount = ($price/6)*10;

											@endphp
											<strike>{{ $amount }}</strike>
											<br>Discount: 40% off</h4>
										<h4 style="color:red;font-size: 20px;margin-top: 10px;">Selling Price: Rs{{ $show->detail->amount }}</h4><br>
										<br>
										@if($show->status==1)
											<a href="{{ url('single',$show->id) }}">Buy Now</a>
											@if(\Illuminate\Support\Facades\Auth::check())
												<a href="{{ url('cart',$show->id) }}" class="btn9">Add to Cart</a>
											@else
												<a href="#" data-toggle="modal" data-target="#myModal11" class="btn9">Add to Cart</a>
											@endif
										@else
											<a href="#" class="btn btn-info disabled btn22" type="button">Sold Out</a>
										@endif
									</div>
								</div>
							@endif
						</div>
					@endif
					@if ($loop->iteration == '8' || $loop->iteration == '11' )
						<div class="new-items new_middle noone">
							@if($loop->iteration == 8)
								<div class="item2">
									<a href="{{ url('single',$show->id) }}"><img src="{{ asset('app/'.$show->image) }}" alt=""/></a>
									<div class="item-info2">
										<h4><a href="{{ url('single',$show->id) }}">{{ $show->name }}</a></h4><br>
										<p class="qty">Size :: {{ $show->height }} X {{ $show->width }}</p><br>
										<h4>Rs:
											@php
												$price = $show->detail->amount;
                                                $amount = ($price/6)*10;

											@endphp
											<strike>{{ $amount }}</strike>
											<br>Discount: 40% off</h4>
										<h4 style="color:red;font-size: 20px;margin-top: 10px;">Selling Price: Rs{{ $show->detail->amount }}</h4><br>
										@if($show->status==1)
											<a href="{{ url('single',$show->id) }}">Buy Now</a>
											@if(\Illuminate\Support\Facades\Auth::check())
												<a href="{{ url('cart',$show->id) }}" class="btn9">Add to Cart</a>
											@else
												<a href="#" data-toggle="modal" data-target="#myModal11" class="btn9">Add to Cart</a>
											@endif
										@else
											<a href="#" class="btn btn-info disabled btn22" type="button">Sold Out</a>
										@endif
									</div>
								</div>
							@endif
							@if($loop->iteration == 11)
								<div class="item5">
									<a href="{{ url('single',$show->id) }}"><img src="{{ asset('app/'.$show->image) }}" alt=""/></a>
									<div class="item-info5">
										<h4><a href="{{ url('single',$show->id) }}">{{ $show->name }}</a></h4><br>
										<p class="qty">Size :: {{ $show->height }} X {{ $show->width }}</p><br>
										<h4>Rs:
											@php
												$price = $show->detail->amount;
                                                $amount = ($price/6)*10;

											@endphp
											<strike>{{ $amount }}</strike>
											<br>Discount: 40% off</h4>
										<h4 style="color:red;font-size: 20px;margin-top: 10px;">Selling Price: Rs{{ $show->detail->amount }}</h4><br>
										@if($show->status==1)
											<a href="{{ url('single',$show->id) }}">Buy Now</a>
											@if(\Illuminate\Support\Facades\Auth::check())
												<a href="{{ url('cart',$show->id) }}" class="btn9">Add to Cart</a>
											@else
												<a href="#" data-toggle="modal" data-target="#myModal11" class="btn9">Add to Cart</a>
											@endif
										@else
											<a href="#" class="btn btn-info disabled btn22" type="button">Sold Out</a>
										@endif
									</div>
								</div>
							@endif
						</div>
					@endif
					@if ($loop->iteration == '9' || $loop->iteration == '12' )
						<div class="new-items new_last noone">
							@if($loop->iteration == 9)
								<div class="item3">
									<a href="{{ url('single',$show->id) }}"><img src="{{ asset('app/'.$show->image) }}" alt=""/></a>
									<div class="item-info3">
										<h4><a href="{{ url('single',$show->id) }}">{{ $show->name }}</a></h4><br>
										<p class="qty">Size :: {{ $show->height }} X {{ $show->width }}</p><br>
										<h4>Rs:
											@php
												$price = $show->detail->amount;
                                                $amount = ($price/6)*10;

											@endphp
											<strike>{{ $amount }}</strike>
											<br>Discount: 40% off</h4>
										<h4 style="color:red;font-size: 20px;margin-top: 10px;">Selling Price: Rs{{ $show->detail->amount }}</h4><br>
										@if($show->status==1)
											<a href="{{ url('single',$show->id) }}">Buy Now</a>
											@if(\Illuminate\Support\Facades\Auth::check())
												<a href="{{ url('cart',$show->id) }}" class="btn9">Add to Cart</a>
											@else
												<a href="#" data-toggle="modal" data-target="#myModal11" class="btn9">Add to Cart</a>
											@endif
										@else
											<a href="#" class="btn btn-info disabled btn22" type="button">Sold Out</a>
										@endif
									</div>
								</div>
							@endif
							@if($loop->iteration == 12)
								<div class="item6">
									<a href="{{ url('single',$show->id) }}"><img src="{{ asset('app/'.$show->image) }}" alt=""/></a>
									<div class="item-info6">
										<h4><a href="{{ url('single',$show->id) }}">{{ $show->name }}</a></h4><br>
										<p class="qty">Size :: {{ $show->height }} X {{ $show->width }}</p><br>
										<h4>Rs:
											@php
												$price = $show->detail->amount;
                                                $amount = ($price/6)*10;

											@endphp
											<strike>{{ $amount }}</strike>
											<br>Discount: 40% off</h4>
										<h4 style="color:red;font-size: 20px;margin-top: 10px;">Selling Price: Rs{{ $show->detail->amount }}</h4><br>
										@if($show->status)
											<a href="{{ url('single',$show->id) }}">Buy Now</a>
											@if(\Illuminate\Support\Facades\Auth::check())
												<a href="{{ url('cart',$show->id) }}" class="btn9">Add to Cart</a>
											@else
												<a href="#" data-toggle="modal" data-target="#myModal11" class="btn9">Add to Cart</a>
											@endif
										@else
											<a href="#" class="btn btn-info disabled btn22" type="button">Sold Out</a>
										@endif
									</div>
								</div>
							@endif
						</div>
					@endif
			@if($loop->iteration == 9 || $loop->iteration == 12)
				<div class="clearfix"></div>
			@endif
				@endforeach
		</div>
	</div>
</div>

<div class="portfolio" id="port">
	<div class="port-head">
		<h3 class="tittle">Portfolio</h3>
	</div>
	<ul class="simplefilter">
		<li class="active" data-filter="all">All</li>
		<li data-filter="1">Living Room</li>
		<li data-filter="2">Bed Room</li>
		<li data-filter="3">Kitchen</li>
		<li data-filter="4">Study Room</li>

	</ul>

	<div class="filtr-container">
		@foreach($portfolio as $port)
		@if($loop->iteration == 1)
		<div class="  filtr-item gallery-t" data-category="2" data-sort="Busy streets">
			<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

				<figure>
					<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
						<h3>{{ $port->name }}</h3>
						<a href="{{ url('single',$port->id) }}" class="a1" ><button class="btn btn-primary">Buy Now</button></a>
						@if(\Illuminate\Support\Facades\Auth::check())
						<a href="{{ url('cart',$port->id) }}" class="a1" ><button class="btn btn-success">Add to Cart</button></a>
						@else
							<a href="#" data-toggle="modal" data-target="#myModal11" class="a1" ><button class="btn btn-success">Add to Cart</button></a>
							@endif
					</figcaption>
				</figure>
			</a>
		</div>
		@endif
		@if($loop->iteration == 2)
		<div class=" filtr-item" data-category="1" data-sort="Luminous night">
			<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

				<figure>
					<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
						<h3>{{ $port->name }}</h3>
						<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
						@if(\Illuminate\Support\Facades\Auth::check())
							<a href="{{ url('cart',$port->id) }}" ><button class="btn btn-success">Add to Cart</button></a>
						@else
							<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
						@endif
					</figcaption>
				</figure>
			</a>

		</div>
		@endif
		@if($loop->iteration == 3)
		<div class=" filtr-item" data-category="4" data-sort="City wonders">
			<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

				<figure>
					<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
						<h3>{{ $port->name }}</h3>
						<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
						@if(\Illuminate\Support\Facades\Auth::check())
							<a href="{{ url('cart',$port->id) }}" ><button class="btn btn-success">Add to Cart</button></a>
						@else
							<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
						@endif
					</figcaption>
				</figure>
			</a>

		</div>
		@endif
		@if($loop->iteration == 4)
		<div class="  filtr-item" data-category="3" data-sort="In production">
			<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

				<figure>
					<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
						<h3>{{ $port->name }}</h3>
						<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
						@if(\Illuminate\Support\Facades\Auth::check())
							<a href="{{ url('cart',$port->id) }}" ><button class="btn btn-success">Add to Cart</button></a>
						@else
							<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
						@endif
					</figcaption>
				</figure>
			</a>

		</div>
		@endif
		@if($loop->iteration == 5)
		<div class=" filtr-item" data-category="2" data-sort="Industrial site">
			<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

				<figure>
					<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
						<h3>{{ $port->name }}</h3>
						<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
						@if(\Illuminate\Support\Facades\Auth::check())
							<a href="{{ url('cart',$port->id) }}" ><button class="btn btn-success">Add to Cart</button></a>
						@else
							<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
						@endif
					</figcaption>
				</figure>
			</a>

		</div>
		@endif
		@if($loop->iteration == 6)
		<div class=" filtr-item" data-category="1" data-sort="Peaceful lake">
			<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

				<figure>
					<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
						<h3>{{ $port->name }}</h3>
						<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
						@if(\Illuminate\Support\Facades\Auth::check())
							<a href="{{ url('cart',$port->id) }}" ><button class="btn btn-success">Add to Cart</button></a>
						@else
							<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
						@endif
					</figcaption>
				</figure>
			</a>

		</div>
		@endif
		@if($loop->iteration == 7)
		<div class="  filtr-item" data-category="1" data-sort="City lights">
			<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

				<figure>
					<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
						<h3>{{ $port->name }}</h3>
						<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
						@if(\Illuminate\Support\Facades\Auth::check())
							<a href="{{ url('cart',$port->id) }}" ><button class="btn btn-success">Add to Cart</button></a>
						@else
							<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
						@endif
					</figcaption>
				</figure>
			</a>

		</div>
		@endif
		@if($loop->iteration == 8)
		<div class=" filtr-item" data-category="1" data-sort="Dreamhouse">
			<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

				<figure>
					<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
						<h3>{{ $port->name }}</h3>
						<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
						@if(\Illuminate\Support\Facades\Auth::check())
							<a href="{{ url('cart',$port->id) }}" ><button class="btn btn-success">Add to Cart</button></a>
						@else
							<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
						@endif
					</figcaption>
				</figure>
			</a>

		</div>
		@endif
		@if($loop->iteration == 9)
		<div class=" filtr-item" data-category="2" data-sort="Dreamhouse">
			<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

				<figure>
					<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
						<h3>{{ $port->name }}</h3>
						<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
						@if(\Illuminate\Support\Facades\Auth::check())
							<a href="{{ url('cart',$port->id) }}"><button class="btn btn-success">Add to Cart</button></a>
						@else
							<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
						@endif
					</figcaption>
				</figure>
			</a>

		</div>
		@endif
			@if($loop->iteration == 10)
				<div class=" filtr-item" data-category="2" data-sort="Dreamhouse">
					<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

						<figure>
							<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
								<h3>{{ $port->name }}</h3>
								<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
								@if(\Illuminate\Support\Facades\Auth::check())
									<a href="{{ url('cart',$port->id) }}"><button class="btn btn-success">Add to Cart</button></a>
								@else
									<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
								@endif
							</figcaption>
						</figure>
					</a>

				</div>
			@endif
			@if($loop->iteration == 11)
				<div class=" filtr-item" data-category="4" data-sort="Dreamhouse">
					<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

						<figure>
							<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
								<h3>{{ $port->name }}</h3>
								<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
								@if(\Illuminate\Support\Facades\Auth::check())
									<a href="{{ url('cart',$port->id) }}"><button class="btn btn-success">Add to Cart</button></a>
								@else
									<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
								@endif
							</figcaption>
						</figure>
					</a>

				</div>
			@endif
			@if($loop->iteration == 12)
				<div class=" filtr-item" data-category="1" data-sort="Dreamhouse">
					<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

						<figure>
							<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
								<h3>{{ $port->name }}</h3>
								<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
								@if(\Illuminate\Support\Facades\Auth::check())
									<a href="{{ url('cart',$port->id) }}"><button class="btn btn-success">Add to Cart</button></a>
								@else
									<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
								@endif
							</figcaption>
						</figure>
					</a>

				</div>
			@endif
			@if($loop->iteration == 13)
				<div class=" filtr-item" data-category="2" data-sort="Dreamhouse">
					<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

						<figure>
							<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
								<h3>{{ $port->name }}</h3>
								<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
								@if(\Illuminate\Support\Facades\Auth::check())
									<a href="{{ url('cart',$port->id) }}"><button class="btn btn-success">Add to Cart</button></a>
								@else
									<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
								@endif
							</figcaption>
						</figure>
					</a>

				</div>
			@endif
			@if($loop->iteration == 14)
				<div class=" filtr-item" data-category="1" data-sort="Dreamhouse">
					<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

						<figure>
							<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
								<h3>{{ $port->name }}</h3>
								<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
								@if(\Illuminate\Support\Facades\Auth::check())
									<a href="{{ url('cart',$port->id) }}"><button class="btn btn-success">Add to Cart</button></a>
								@else
									<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
								@endif
							</figcaption>
						</figure>
					</a>

				</div>
			@endif
			@if($loop->iteration == 15)
				<div class=" filtr-item" data-category="4" data-sort="Dreamhouse">
					<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

						<figure>
							<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
								<h3>{{ $port->name }}</h3>
								<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
								@if(\Illuminate\Support\Facades\Auth::check())
									<a href="{{ url('cart',$port->id) }}"><button class="btn btn-success">Add to Cart</button></a>
								@else
									<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
								@endif
							</figcaption>
						</figure>
					</a>

				</div>
			@endif
			@if($loop->iteration == 16)
				<div class=" filtr-item" data-category="4" data-sort="Dreamhouse">
					<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

						<figure>
							<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
								<h3>{{ $port->name }}</h3>
								<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
								@if(\Illuminate\Support\Facades\Auth::check())
									<a href="{{ url('cart',$port->id) }}"><button class="btn btn-success">Add to Cart</button></a>
								@else
									<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
								@endif
							</figcaption>
						</figure>
					</a>

				</div>
			@endif
			@if($loop->iteration == 17)
				<div class=" filtr-item" data-category="2" data-sort="Dreamhouse">
					<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

						<figure>
							<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
								<h3>{{ $port->name }}</h3>
								<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
								@if(\Illuminate\Support\Facades\Auth::check())
									<a href="{{ url('cart',$port->id) }}"><button class="btn btn-success">Add to Cart</button></a>
								@else
									<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
								@endif
							</figcaption>
						</figure>
					</a>

				</div>
			@endif
			@if($loop->iteration == 18)
				<div class=" filtr-item" data-category="1" data-sort="Dreamhouse">
					<a href="{{ asset('app/'.$port->image) }}" rel="title" class="b-link-stripe b-animate-go  thickbox">

						<figure>
							<img src="{{ asset('app/'.$port->image) }}" class="img-responsive" alt=" " />	<figcaption class="a11">
								<h3>{{ $port->name }}</h3>
								<a href="{{ url('single',$port->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
								@if(\Illuminate\Support\Facades\Auth::check())
									<a href="{{ url('cart',$port->id) }}"><button class="btn btn-success">Add to Cart</button></a>
								@else
									<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
								@endif
							</figcaption>
						</figure>
					</a>

				</div>
			@endif
		@if($loop->last)
		<div class="clearfix"> </div>
		@endif
			@endforeach
	</div>

</div>

<!-- //Portfolio -->


<!-- projects -->
<div class="projects" id="projects">
	<div class="container">
		<div class="port-head">
			<h3 class="tittle">Latest Designs</h3>

		</div>
	</div>
	<div class="projects-grids">
		<div class="sreen-gallery-cursual">

			<div id="owl-demo" class="owl-carousel owl-theme">
			@foreach($latest_design as $latest)
				<div class="item">
					<div class="projects-agile-grid-info">
						<img src="{{ asset('app/'.$latest->image) }}" alt="" />
						<div class="projects-grid-caption">

							<h4>{{ $latest->name }}</h4>
							<a href="{{ url('single',$latest->id) }}" class="a1"><button class="btn btn-primary">Buy Now</button></a>
							@if(\Illuminate\Support\Facades\Auth::check())
								<a href="{{ url('cart',$latest->id) }}" class="a1"><button class="btn btn-success">Add to Cart</button></a>
							@else
								<a href="#" data-toggle="modal" data-target="#myModal11" class="a1"><button class="btn btn-success">Add to Cart</button></a>
							@endif
						</div>
					</div>
				</div>
				@endforeach
			</div>
				
		</div>
	</div>
</div>
<!-- //projects -->
<div class="container" style="margin-top: 50px;">
	<div class="testimonial-icon">
		<h2 style="text-align: center;">Testimonial</h2>
		<img src="https://www.docopd.com/images/testimonial-icon.png"> </div>
	<div id="ninja-slider">
		<div class="slider-inner">
			<ul>
				<li>
					<div class="content">
						<div class="user-testimonial">
							<p> <q>Preeti Thaker Arora's paintings are absolutely fantastic and has brightened up my new home..Love them </q> </p>
							</p>
						</div>
						<div class="testimonial_username"> - Sonia Mitra </div>
					</div>
				</li>
				<li>
					<div class="content">
						<div class="user-testimonial">
							<p> <q>I bought Preeti’s paintings for my office Cabin and it refreshes my day every time I see it. I strongly recommend to buy her artworks </q> </p>
							</p>
						</div>
						<div class="testimonial_username"> - Yogesh Sahu </div>
					</div>
				</li>
				<li>
					<div class="content">
						<div class="user-testimonial">
							<p> <q>Preeti Thaker Arora Art is a brilliant innovation, particularly for collectors without deep pockets. The site is also delightfully subversive because, in so many instances, the quality of the work beats out many a pricey offering from the average blue-chip gallery. </q> </p>
							</p>
						</div>
						<div class="testimonial_username"> – Amit Sharma </div>
					</div>
				</li>
				<li>
					<div class="content">
						<div class="user-testimonial">
							<p> <q>I was really impressed with the quality of the work that I received from Preeti Thaker Arora Art. I love knowing that I’m supporting artists from around the world.</q> </p>
							</p>
						</div>
						<div class="testimonial_username"> – Niharika C. </div>

					</div>
				</li>
				<li>
					<div class="content">
						<div class="user-testimonial">
							<p> <q>Having my works at Preeti Thaker Arora Art has been one of the greatest gifts I have had. You helped me in so many ways in my career and a very special opportunity was given to me thanks to you. </q> </p>
						</div>
						<div class="testimonial_username"> - Mahesh Gupta  </div>

					</div>
				</li>
				<li>
					<div class="content">
						<div class="user-testimonial">
							<p> <q>...From seeing a piece of art that I liked to hanging it on my wall, all aspects were easy and it felt like I had a personal assistant throughout the whole process.  </q> </p>
						</div>
						<div class="testimonial_username"> – Prachi Ram Das  </div>
					</div>

				</li>
			</ul>
			<div class="fs-icon" title="Expand/Close"></div>
		</div>
	</div>
</div>

<div class="w3l-footer" id="contact">
	<div class="container">
		<div class="footer-info-agile">

			<div class="col-md-4 footer-info-grid address">
				<h4>ADDRESS</h4>
				<address>
					<ul>
						<li>2a c wing Crystal plaza,</li>
						<li>Opp. Infinity mall </li>
						<li>Andheri link Road,</li>
						<li>Andheri west,
							Mumbai 53</li>
						<li>Telephone : +91 9920969994</li>
						<li>Email : <a class="mail" href="https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession">thepeoplesplace1476@yahoo.com</a></li>
					</ul>
				</address>
			</div>
			<div class="col-md-4 footer-info-grid links">
				<h4>QUICK LINKS</h4>
				<ul>
					<li><a class="" href="{{ url('about-us') }}">About Us</a></li>
					<li><a class="" href="{{ url('talk_town') }}">Talk of the town</a></li>
					<li><a class="" href="{{ url('signature') }}">Signature collection</a></li>
					<li><a class="" href="{{ url('elite') }}">Elite Cosmic collection</a><li>
					<li><a class="" href="#contact">Contact</a><li>
				</ul>
			</div>

			<div class="col-md-4 footer-grid newsletter connect-social">
				<h4>Get in Touch</h4>
				<section class="social">
					<ul>
						<li><a class="icon fb" href="https://www.facebook.com/preetithakerarora/"><i class="fa fa-facebook"></i></a></li>
						<li><a class="icon tw" href="https://twitter.com/ThakerArora?s=08"><i class="fa fa-twitter"></i></a></li>
						<li><a class="icon db" href="#"><i class="fa fa-dribbble"></i></a></li>
						<li><a class="icon gp" href="#"><i class="fa fa-google-plus"></i></a></li>


					</ul>
				</section>

				<div class="clearfix"></div>
			</div>

			<div class="clearfix"></div>
		</div>

		<div class="copyright-wthree">
			<p>&copy; 2017 STYLISH DECOR. All Rights Reserved | Design by <a href="https://www.letscodeprivatelimited.com/"> LETS CODE PRIVATE LIMITED</a></p>
		</div>

	</div>
</div>

<script type="text/javascript" src="{{asset('front/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('front/js/bootstrap.js')}}"></script>
  <!-- smooth scrolling -->

{{--<script type="text/javascript">--}}
	{{--$(document).ready(function() {--}}
		{{--myFunction();--}}
	{{--/*--}}
		{{--var defaults = {--}}
		{{--containerID: 'toTop', // fading element id--}}
		{{--containerHoverID: 'toTopHover', // fading element hover id--}}
		{{--scrollSpeed: 1200,--}}
		{{--easingType: 'linear'--}}
		{{--};--}}
	{{--*/--}}
	{{--$().UItoTop({ easingType: 'easeOutQuart' });--}}
	{{--});--}}
{{--</script>--}}
@if (count($errors) > 0)
	<script>
		$(document).ready(function () {
			$('#errormodal').modal('show');
		});
	</script>
@endif
{{--<script>--}}
	{{--function myFunction() {--}}
        {{--var tips = [--}}
            {{--"Ananya Bhatiya Purchased Talk of the town",--}}
            {{--"Rahul Tripathi Purchased Elite Cosmic",--}}
            {{--"Raghvendra Chauhan Purchased Signature Collection",--}}
            {{--"Annie Verma Purchased Elite Cosmic",--}}
            {{--"Meghna Chauhan Purchased Talk of the town",--}}
            {{--"Mukul Bhadana Purchased Elite Cosmic",--}}
            {{--"Ajay Garg Purchased Signature Collection"--}}
        {{--];--}}
		{{--var x = document.getElementById("snackbar")--}}
		{{--x.className = "show";--}}
{{--//		setTimeout(function(){--}}
		    {{--setInterval(function() {--}}
            {{--var i = Math.round((Math.random()) * tips.length);--}}
{{--//var i = Math.round((1) * tips.length);--}}
            {{--if (i == tips.length) --i;--}}
            {{--$("#snackbar").html(tips[i]);--}}
            {{--var x = document.getElementById("snackbar")--}}
            {{--x.className = "show";--}}
            {{--x.className = x.className.replace("show", "");--}}
        {{--}, 5 * 1000);--}}
{{--//		 }, 3000);--}}
	{{--}--}}
{{--</script>--}}
	<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- js -->

<!--banner Slider starts Here-->
						<script src="{{asset('front/js/responsiveslides.min.js')}}"></script>
<script>
    // You can also use "$(window).load(function() {"
    $(function () {
        // Slideshow 4
        $("#slider3").responsiveSlides({
            auto: true,
            pager:true,
            nav:false,
            speed: 500,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });

    });
</script>
<script>
    // You can also use "$(window).load(function() {"
    $(function () {
        // Slideshow 4
        $("#slider").responsiveSlides({
            auto: true,
            pager:false,
            nav:false,
            speed: 500,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });

    });
</script>
<script>
    // You can also use "$(window).load(function() {"
    $(function () {
        // Slideshow 4
        $("#slider4").responsiveSlides({
            auto: true,
            pager:false,
            nav:true,
            speed: 500,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });

    });
</script>
<!--banner Slider starts Here-->
<!-- PopUp-Box-JavaScript -->
			<script src="{{asset('front/js/jquery.chocolat.js')}}"></script>
			<script type="text/javascript">
				$(function() {
					$('.filtr-item a').Chocolat();
				});
			</script>
		<!-- //PopUp-Box-JavaScript -->
		<!-- start-smoth-scrolling -->
<script type="text/javascript" src="{{asset('front/js/move-top.js')}}"></script>
<script type="text/javascript" src="{{asset('front/js/easing.js')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<!-- Tabs-JavaScript -->
			<script src="{{asset('front/js/jquery.filterizr.js')}}"></script>
			<script src="{{asset('front/js/controls.js')}}"></script>
			<script type="text/javascript">
				$(function() {
					$('.filtr-container').filterizr();
				});
			</script>
		<!-- //Tabs-JavaScript -->

<script src="{{asset('front/js/owl.carousel.js')}}"></script>
<link href="{{asset('front/css/owl.theme.css')}}" rel="stylesheet">
<script src="{{asset('front/js/ninja-slider.js')}}"  type="text/javascript"></script>

<script>
$(document).ready(function() {


	$("#owl-demo").owlCarousel({
 
		autoPlay: 3000, //Set AutoPlay to 3 seconds
		  autoPlay : true,
		   navigation :true,

		items : 4,
		itemsDesktop : [640,5],
		itemsDesktopSmall : [414,4]
 
	});
	
}); 
</script>
@if(session('alerts'))
	<script>
		$(document).ready(function () {
            swal(
                'Congratulations !',
                'Your order has been placed successfully',
                'success'
            )
		});
	</script>
@endif
@if (session('status'))
	<script>
        $(document).ready(function () {
            swal(
                'Congratulations !',
                '{{ session('status') }}',
                'success'
            )
        });
	</script>
@endif
</body>
</html>