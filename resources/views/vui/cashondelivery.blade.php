<!DOCTYPE HTML>
<html>
<head>
    <title>Stylish Decor-by preeti thaker arora</title>
    <link href="{{asset('front/css/bootstrap.css')}}" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary JavaScript plugins) -->
    <script type='text/javascript' src="{{asset('front/js/jquery-1.11.1.min.js')}}"></script>
    <!-- Custom Theme files -->

    <!-- Custom Theme files -->
    <!--//theme-style-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href='http://fonts.googleapis.com/css?family=Montserrat|Raleway:400,200,300,500,600,700,800,900,100' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Aladin' rel='stylesheet' type='text/css'>
    <link href="{{asset('front/css/style.css')}}" rel='stylesheet' type='text/css' />
    <!-- start menu -->
    <link href="{{asset('front/css/megamenu.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="{{asset('front/js/jquery-2.1.4.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/js/megamenu.js')}}"></script>
    <script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
    <script src="{{asset('front/js/materialize.min.js')}}"></script>
    <script src="{{asset('front/js/menu_jquery.js')}}"></script>
    <script src="{{asset('front/js/simpleCart.min.js')}}"> </script>
    <script src="{{asset('front/js/bootstrap.js')}}"></script>
    <link href="{{asset('front/css/form.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('front/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="all">
    <link href="{{asset('front/css/materialize.min.css')}}" rel="stylesheet" type="text/css" >

    <script>
//        var options = {
//
//            url: "resources/countries.json",
//
//            getValue: "name",
//
//            list: {
//                match: {
//                    enabled: true
//                }
//            },
//
//            theme: "square"
//        };
//
//        $("#countries").easyAutocomplete(options);
    </script>
</head>
<body>
<div class="top_bg">
    <div class="container-fluid">
        <div class="header_top-sec">
            <div class="top_right">
                <ul>
                    <li><a href="#">help</a></li>|
                    <li><a href="#contact">Contact</a></li>|
                </ul>
            </div>
            <div class="top_left">
                <div class="col-md-5">
                    <ul>
                        <li>Email : <a class="mail" href="https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession">thepeoplesplace1476@yahoo.com</a></li>|
                    </ul>
                </div>
                <div class="social col-md-4 col-xs-6">
                    <ul>
                        <li><a href="https://www.facebook.com/preetithakerarora/"><i class="facebook"></i></a></li>
                        <li><a href="https://twitter.com/ThakerArora?s=08"><i class="twitter"></i></a></li>
                        <li><a href="#"><i class="dribble"></i></a></li>
                        <li><a href="#"><i class="google"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-6">
                    @if(\Illuminate\Support\Facades\Auth::check())
                        <ul>
                            <li class="dropdown tag one">
                                <a href="#" class="dropdown-toggle" id="drop1" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-tag"></span> {{ \Illuminate\Support\Facades\Auth::user()->name}}</a>
                                <ul class="dropdown-menu" aria-labelledby="drop1">
                                    <li><a href="{{url('myaccount')}}">My Account</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{url('myorder')}}">My Order</a></li>
                                </ul>
                            </li>
                            <li class="tag">
                                <a  href="{{ url('/logout') }}"><span class="glyphicon glyphicon-log-in"></span>Logout</a>
                            </li>
                        </ul>
                    @else
                        <ul>
                            <li class="tag one"><a href="#" data-toggle="modal" data-target="#myModal11"><span class="glyphicon glyphicon-tag"></span> Login</a></li>
                            <li class="tag"><a href="#" data-toggle="modal" data-target="#myModal12"><span class="glyphicon glyphicon-log-in"></span> Sign Up</a></li>
                        </ul>
                    @endif
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal11" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-info">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body modal-spa">
                <div class="login-grids">
                    <div class="login-bottom">
                        <h3>Login</h3>
                        <form action="{{ url('/login') }}" method="post">
                            {{ csrf_field() }}
                            <input type="text" class="user" name="email" placeholder="Email" required="">
                            <input type="password" name="password" placeholder="Password" required="">
                            <input type="submit" value="Submit">
                            &nbsp;
                            <a href="#" data-toggle="modal" data-target="#myModal12">Create A New Account?</a><br><br>
                            <a href="{{url('forget')}}">Forgot Password</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //login -->
<!--/signup -->
<div class="modal fade" id="myModal12" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-info">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body modal-spa">
                <div class="login-grids">
                    <div class="login-bottom">
                        <h3>Sign Up</h3>
                        <form action="{{ url('/register') }}" method="post">
                            {{ csrf_field() }}
                            <input type="text" name="name" placeholder="Your Name" required="">
                            <input type="email" name="email" placeholder="Your Email" required="">
                            <input type="tel" name="mobile" placeholder="Mobile" required="">
                            <input type="password" name="password" placeholder="Password" required="">
                            <input type="password" name="password_confirmation" placeholder="Confirm Password" required="">
                            <div class="signup">
												<span class="agree-checkbox">
													<label class="checkbox"><input type="checkbox" name="checkbox">I agree to your <a class="w3layouts-t" href="#">Terms of Use</a></label>
												</span>
                            </div>
                            <div class="clearfix"></div>
                            <input type="submit" value="Sign Up">
                            &nbsp;<a href="#" data-toggle="modal" data-target="#myModal11">Already A Member?</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="header_top">
    <div class="container">
        <div class="logo">
            <a href="{{ url('/') }}"><img src="{{asset('front/images/logo.png')}}" alt=""/></a>
        </div>
        <div class="header_right">
            <div class="cart box_1">
                @if(isset($_SESSION['product']))
                    <a href="{{ url('cart') }}">
                        <h3> <span class="">{{ count($_SESSION['product']) }} items</span> <img src="{{asset('front/images/bag2.png')}}" alt=""></h3>
                    </a>
                @endif
                {{--<p><a href="javascript:;" class="simpleCart_empty">Empty cart</a></p>--}}
                <div class="clearfix"> </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
</div>
<!--cart-->

<!------>
<div class="mega_nav">
    <div class="container">
        <div class="menu_sec">
            <!-- start header menu -->
            <ul class="megamenu skyblue">
                <li class="active grid"><a class="color1" href="{{ url('/') }}">Home</a></li>
                <li class=" grid"><a class="color1" href="{{ url('about-us') }}">About Us</a></li>
                <li class=" grid"><a class="color1" href="{{ url('media') }}">Media</a></li>
                <li class=" grid"><a class="color1" href="{{ url('/') }}">Portfolio</a></li>
                <li class=" grid"><a class="color1" href="{{ url('/') }}">Corporate Gifting</a></li>
                <li class=" grid"><a class="color1" href="{{url('charity')}}">Charity</a></li>
                <li class=" grid"><a class="color1" href="{{ url('contact') }}">Contact us</a></li>
            </ul>



            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!---->
<!--header//-->
<!--header//-->
<div class="container" style="margin: 50px auto;">
    <h3>Secure Checkout</h3>
    <hr style="border: 1px solid #d0d0d0;"/>
    <form class="col s12" action="{{ url('place-order') }}" method="post">
        <ul class="collapsible popout" data-collapsible="accordion">

            <li>
                <div class="collapsible-header"><i class="material-icons">filter_drama</i>Account Details</div>
                <div class="collapsible-body">
                    <div class="row">
                        <div class="input-field col m12 s12">
                            <input type="text" name="first_name" id="firstname" value="{{ Auth::user()->name ?? '' }}" required/>
                            <label for="first_name">First Name</label>
                        </div>

                    </div>
                    <div class="row">
                        <div class="input-field col m6 s12">
                            <input type="email" name="email" id="email" required value="{{ Auth::user()->email ?? '' }}"/>
                            <label for="last_name">Email</label>
                        </div>
                        <div class="input-field col m6 s12">
                            <input type="text" name="mobile" id="phone" required />
                            <label for="disabled">Phone No.</label>
                        </div>
                    </div>
                    <div class="row">
                        <button type="button" id="step1" class="waves-effect waves-light btn"> Proceed</button>
                    </div>
                </div>
            </li>
            <li>
                <div class="collapsible-header"><i class="material-icons">place</i>Billing Details</div>
                <div class="collapsible-body"><div class="row">
                        <div class="input-field col m12 s12">
                            <input type="text" name="last_name" id="lastname" required  />
                            <label for="email">Last Name</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6 s12">
                            <input type="text" name="address1" required  />
                            <label>Address 1</label>
                        </div>
                        <div class="input-field col m6 s12">
                            <input type="text" name="address2" required  />
                            <label>Address 2</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6 s12">
                            <input type="text" name="city" required  />
                            <label>City</label>
                        </div>
                        <div class="input-field col m6 s12">
                            <input type="text" name="state" required  />
                            <label>State</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6 s12">
                            <input type="text" name="country" required  />
                            <label>Country</label>
                        </div>
                        <div class="input-field col m6 s12">
                            <input type="tel" name="zipcode" required  />
                            <label>Zip Code</label>
                        </div>
                    </div>
                    <div class="row">
                        <button type="button" id="step2" class="waves-effect waves-light btn"> Proceed</button>
                    </div>
                </div>
            </li>
            <li>
                <div class="collapsible-header"><i class="material-icons">picture_in_picture</i>Frames</div>
                <div class="collapsible-body">
                    <div class="row">
                        <form action="#">
                            <p>
                                <input type="radio" id="test5" name="frame" value="950" />
                                <label for="test5">Wooden bracket frame</label>
                            </p>
                            <p>
                                <input type="radio" id="test6" name="frame" value="0" />
                                <label for="test6">Unframed</label>
                            </p>
                        </form>
                    </div>
                    <div class="row">
                        <button type="button" id="step3" class="waves-effect waves-light btn"> Proceed</button>
                    </div>
                </div>
            </li>
            <li>
                <div class="collapsible-header"><i class="material-icons">whatshot</i>Order Confirmation</div>
                <div class="collapsible-body cd">

                    <div class="cart_main">
                            {{--code to show cart items starts here--}}
                            <div class="cart-items">
                                <?php
                                $total = '0';
                                ?>
                                <h2>My Shopping Bag ({{ $count == 1 ? $count = $count : $count = $count-1  }} Items)</h2>
                                @if($count>0)
                                    @if(count($cart)>0)
                                        @foreach($cart as $datas)

                                            <script>$(document).ready(function(c) {
                                                    $('.datas{{$datas->id}}').on('click', function(c){
                                                        $.ajax({url: "{{ url('delete-cart',$datas->cartid) }}", success: function(result){
                                                            location.reload();
                                                        }});
                                                    });
                                                });
                                            </script>

                                            <div class="cart-header">
                                                <div class="cart-sec">
                                                    <div class="cart-item cyc">
                                                        <img src="{{ asset('app/'.$datas->image ?? '')  }}"/>
                                                    </div>
                                                    <div class="cart-item-info">
                                                        <h3>{{ $datas->name }}</h3>
                                                        <h4><span>Rs. </span>{{ $datas->amount ?? 5000 }}</h4>
                                                        <p class="qty">Size :: {{ $datas->height }} X {{ $datas->width }}</p>

                                                        {{--<input min="1" type="number" id="quantity" name="quantity" value="1" class="form-control input-small">--}}
                                                    </div>
                                                    <input type="hidden" name="cart_items[]" value="{{ $datas->product_id }}" >
                                                    <div class="clearfix"></div>
                                                    <div class="delivery">
                                                        {{--<p>Service Charges:: Rs.50.00</p>--}}
                                                    </div>
                                                    <?php
                                                    $total+=$datas->amount;
                                                    $_SESSION['total']=$total;
                                                    ?>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                @endif
                            </div>
                            {{--code to show cart items ends here--}}

<div class="row">
                            <div class="cart-total">
                                <div class="price-details">
                                    <h3>Price Details</h3>
                                    <span>Total</span>
                                    <span class="total">{{ $total }}</span>
                                    <span>Discount</span>
                                    <span class="total">---</span>
                                    <span>Delivery Charges</span>
                                    <span class="total">300.00</span>
                                    <span>Frame Charges</span>
                                    <span class="total" id="framecharges" >00</span>
                                    <div class="clearfix"></div>
                                </div>
                                <h4 class="last-price ">TOTAL</h4>
                                <span class="total final">{{ $total+300 }}</span>
                                <div class="clearfix"></div>
                                <div class="total-item">
                                </div>
                            </div>
</div>
                    </div>
                    <div class="row pull-right">

                            {{ csrf_field() }}
                            <input type="hidden" name="amount" id="amounts" value="{{ $total+300 ?? 00 }}" />
                            <input type="hidden" name="productinfo" value="Iphone 6C - 16GB" />
                            <input type="hidden" name="surl" value="https:www.pretithakera/success.php"/>
                            <input type="hidden" name="furl" value="https://example.com/failure.php"/>

                            {{--<p>--}}
                                {{--<input type="checkbox" id="test5" />--}}
                                {{--<label for="test5">Terms And Condision</label>--}}
                            {{--</p>--}}
                        <input type="submit" name="submit" value="Proceed" class="waves-effect waves-light btn">

                    </div>
                </div>
            </li>
             </ul>
    </form>
</div>
<div class="w3l-footer" id="contact">
    <div class="container">
        <div class="footer-info-agile">

            <div class="col-md-4 footer-info-grid address">
                <h4>ADDRESS</h4>
                <address>
                    <ul>
                        <li>2a c wing Crystal plaza,</li>
                        <li>Opp. Infinity mall </li>
                        <li>Andheri link Road,</li>
                        <li>Andheri west,
                            Mumbai 53</li>
                        <li>Telephone : +91 9920969994</li>
                        <li>Email : <a class="mail" href="https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession">thepeoplesplace1476@yahoo.com</a></li>
                    </ul>
                </address>
            </div>
            <div class="col-md-4 footer-info-grid links">
                <h4>QUICK LINKS</h4>
                <ul>
                    <li><a  href="{{ url('about-us') }}">About Us</a></li>
                    <li><a  href="{{ url('talk_town') }}">Talk of the town</a></li>
                    <li><a  href="{{ url('signature') }}">Signature collection</a></li>
                    <li><a  href="{{ url('elite') }}">Elite Cosmic collection</a><li>
                    <li><a  href="#contact">Contact</a><li>
                </ul>
            </div>

            <div class="col-md-4 footer-grid newsletter connect-social">
                <h4>Get in Touch</h4>
                <section class="social1">
                    <ul>
                        <li><a class="icon fb" href="https://www.facebook.com/preetithakerarora/"><i class="fa fa-facebook"></i></a></li>
                        <li><a class="icon tw" href="https://twitter.com/ThakerArora?s=08"><i class="fa fa-twitter"></i></a></li>
                        <li><a class="icon db" href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li><a class="icon gp" href="#"><i class="fa fa-google-plus"></i></a></li>


                    </ul>
                </section>

                <div class="clearfix"> </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="copyright-wthree">
            <p>&copy; 2017 STYLISH DECOR. All Rights Reserved | Design by <a href="http://www.letscodeprivatelimited.com/"> LETS CODE PRIVATE LIMITED</a></p>
        </div>

    </div>
</div>
<script>

    {{--$('#frame').on('click',function () {--}}
        {{--var a = this.value;--}}
        {{--console.log(a);--}}
        {{--var b ="{{ $_SESSION['total']+300 }}";--}}
        {{--console.log(b);--}}
        {{--var k =parseInt(b);--}}
        {{--console.log(k);--}}
        {{--var total = a+k;--}}
        {{--console.log('total is '+total);--}}
        {{--$('#amountDiv').html(total);--}}
        {{--//$('')--}}
    {{--});--}}

    $("input[name='frame']").on('change',function () {
        var a = this.value;
        console.log(a);
        var b ="{{ $_SESSION['total']+300 }}";
        console.log(b);
        var k =parseInt(b);
        console.log(k);
        var x =parseInt(a);
        var total = x+k;
        console.log('total is '+total);
        $('.final').html(total);
        $('#framecharges').html(a);
        $('#amounts').value = total;
        //$('')
    });

    $(document).ready(function(){
        $('.collapsible').collapsible();
        $('.collapsible').collapsible('open', 0);

    });

    $('#step1').on('click',function(){
        $('.collapsible').collapsible('open', 1);
    });

    $('#step2').on('click',function(){
        $('.collapsible').collapsible('open', 2);
    });
    $('#step3').on('click',function(){
        $('.collapsible').collapsible('open', 3);
    });
</script>
<!---->
</body>
</html>