<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 07-Feb-17
 * Time: 3:43 PM
 */
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Welcome to Paintings</li>
            <!-- Optionally, you can add icons to the links -->
            <br>

            <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li><a href="{{ url('all-orders') }}"><i class="fa fa-home"></i> <span>Orders</span></a></li>


            {{--Category tree view starts here--}}

            {{--<li class="treeview">--}}
                {{--<a href="#"><i class="fa fa-files-o"></i> <span>Category Section</span>--}}
                    {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ url('add_employees') }}">Add New Category</a></li>--}}
                    {{--<li><a href="{{ url('employees_list') }}">Show All Category</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--Category tree view ends here--}}


            {{--sub-Category tree view starts here--}}
            {{--<li class="treeview">--}}
                {{--<a href="#"><i class="fa fa-edit"></i> <span>Sub Category Section</span>--}}
                    {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ url('add_designation') }}">Add New Sub Category</a></li>--}}
                    {{--<li><a href="{{ url('designation_list') }}">Show All Sub Category</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--sub-Category tree view ends here--}}

            {{--payment mode tree view starts here--}}
            {{--<li class="treeview">--}}
                {{--<a href="#"><i class="fa fa-folder"></i> <span>Payment Section</span>--}}
                    {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{ url('add_project') }}">Add New Payment</a></li>--}}
                    {{--<li><a href="{{ url('projects_list') }}">Show All Payments</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--payment mode tree view ends here--}}


            {{--status tree view starts here--}}
            {{--<li class="treeview">--}}
                {{--<a href="#"><i class="fa fa-users"></i> <span>Status Section</span>--}}
                    {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                     {{--<li><a href="{{ url('/status') }}">Add New Status</a></li>--}}
                    {{--<li><a href="{{ url('/status_list') }}">Show All Status</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--status tree view ends here--}}

            {{--address tree view starts here--}}
            {{--<li class="treeview">--}}
                {{--<a href="#"><i class="fa fa-users"></i> <span>Address Section</span>--}}
                    {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                     {{--<li><a href="{{ url('/address') }}">Add New</a></li>--}}
                    {{--<li><a href="{{ url('/address_list') }}">Show All</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--address tree view ends here--}}



            Product tree view starts here
            <li class="treeview">
                <a href="#"><i class="fa fa-users"></i> <span>Product Section</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                     <li><a href="{{ url('/product') }}">Add New</a></li>
                    <li><a href="{{ url('/product_list') }}">Show All</a></li>
                </ul>
            </li>
            product tree view ends here

            {{--Product detail tree view starts here--}}
            {{--<li class="treeview">--}}
                {{--<a href="#"><i class="fa fa-users"></i> <span>Product Detail Section</span>--}}
                    {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                     {{--<li><a href="{{ url('/product_detail') }}">Add New</a></li>--}}
                    {{--<li><a href="{{ url('/product_detail_list') }}">Show All</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--product detail tree view ends here--}}

        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>