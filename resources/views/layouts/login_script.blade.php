<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 13-Feb-17
 * Time: 10:55 AM
 */
?>


{{--<!-- jQuery Library -->--}}
<script type="text/javascript" src="{{asset('custom/jquery-1.11.2.min.js')}}"></script>
{{--<!--materialize js-->--}}
<script type="text/javascript" src="{{asset('custom/materialize.min.js')}}"></script>
{{--<!--prism-->--}}
<script type="text/javascript" src="{{asset('custom/prism.js')}}"></script>
{{--<!--scrollbar-->--}}
<script type="text/javascript" src="{{asset('custom/perfect-scrollbar.min.js')}}"></script>

{{--<!--plugins.js - Some Specific JS codes for Plugin Settings-->--}}
<script type="text/javascript" src="{{asset('custom/plugins.min.js')}}"></script>
{{--<!--custom-script.js - Add your own theme custom JS-->--}}
<script type="text/javascript" src="{{asset('custom/custom-script.js')}}"></script>
{{--<script type="text/javascript" src="{{asset('dist/js/app.min.js')}}"></script>--}}
@include('layouts.scripts')
