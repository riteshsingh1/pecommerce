<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    
    <title>Paintings</title>

    <!-- Styles -->
    {{--@include('layouts.header')--}}
@include('layouts.adminlte_header')

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    </head>


<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

            @include('layouts.main_header')
            @include('layouts.main_sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">

            @yield('content')

        </section>
        <!--main content end-->

        @include('layouts.control_sidebar')

    </div>
                @include('layouts.main_footer')
    </div>
{{--    @include('layouts.scripts')--}}
    <!-- Scripts -->
    @include('layouts.adminlte_scripts')
</body>
</html>
