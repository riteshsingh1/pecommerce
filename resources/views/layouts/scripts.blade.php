<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 07-Feb-17
 * Time: 3:33 PM
 */
?>


<script src="{{ asset('assets/js/modules/materialadmin/libs/jquery/jquery-migrate-1.2.1.min.js') }} "></script>
<script src="{{ asset('assets/js/modules/materialadmin/libs/bootstrap/bootstrap.min.js') }} "></script>
<script src="{{ asset('assets/js/modules/materialadmin/libs/spin.js/spin.min.js') }} "></script>
<script src="{{ asset('assets/js/modules/materialadmin/libs/autosize/jquery.autosize.min.js') }} "></script>
<script src="{{ asset('assets/js/modules/materialadmin/libs/nanoscroller/jquery.nanoscroller.min.js') }} "></script>
<script src="{{ asset('assets/js/modules/materialadmin/libs/jquery-validation/dist/jquery.validate.min.js') }} "></script>
<script src="{{ asset('assets/js/modules/materialadmin/libs/jquery-validation/dist/additional-methods.min.js') }} "></script>
<script src="{{ asset('assets/js/modules/materialadmin/core/cache/63d0445130d69b2868a8d28c93309746.js') }} "></script>
<script src="{{ asset('assets/js/modules/materialadmin/core/demo/Demo.js') }} "></script>
{{--<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>--}}



