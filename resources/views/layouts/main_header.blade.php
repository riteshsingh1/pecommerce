<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 07-Feb-17
 * Time: 3:44 PM
 */
?>


<!-- Main Header -->
<header class="main-header" style="background-color: #00b1b3">

    <!-- Logo -->
    <a href="{{ url('home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>B</b>P</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Best</b>Paintings</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
               
                    <!-- Menu toggle button -->
                   
                    
                      
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="" class="user-image" alt="User Image">
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="" class="img-circle" alt="User Image">

                            <p>
                                {{ Auth::user()->name }}
                            
                            </p>
                        </li>
                        <!-- Menu Body -->
                    
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="col-md-4 col-md-offset-3">
                                <a href="{{ url('/logout') }}" class="btn btn-danger btn-flat">Sign Out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                    
            </ul>
        </div>
    </nav>
</header>