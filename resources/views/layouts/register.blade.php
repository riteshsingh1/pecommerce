      <!DOCTYPE html>
<html>
<head>
    <title>Paintings Register</title>
    @include('layouts.loginheader')
    @include('layouts.header')
</head>
<body style="background-color: lightgrey">

    <!--Login body start-->

    @yield('content')


    {{--scripts--}}
    @include('layouts.jq')
    @include('layouts.login_script')
    <!--Login body end-->
</body>
</html>