<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 07-Feb-17
 * Time: 5:05 PM
 */
?>



        <!DOCTYPE html>
<html>
<head>
    <title>Paintings Login</title>
    @include('layouts.loginheader')
    @include('layouts.header')
</head>
<body style="background-color: lightgrey">

    <!--Login body start-->

    @yield('content')


    {{--scripts--}}
    @include('layouts.jq')
    @include('layouts.login_script')
    <!--Login body end-->
</body>
</html>

