<?php
    /**
     * Created by PhpStorm.
     * User: Ritesh Singh
     * Date: 1/22/2017
     * Time: 1:18 PM
     */

?>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
