<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 07-Feb-17
 * Time: 3:35 PM
 */
?>

<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        &copy; Created by Letscode Privae Limited
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <a href="#">Company</a>.</strong> All rights reserved.
</footer>
