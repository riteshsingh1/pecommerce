<div>
    <ol class="breadcrumb" style="background-color: transparent">
        @foreach($breadcrumbs as $b)
            @if($loop->last)
                <li><a href="{{ $b['url'] }}">{{ $b['text'] }}</a></li>
            @else
                <li><a class="active" href="{{ $b['url'] }}">{{ $b['text'] }}</a></li>
            @endif
        @endforeach
    </ol>
</div>
