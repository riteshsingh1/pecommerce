<?php
/**
 * Created by PhpStorm.
 */
?>

<br>
            {{--start of head panels--}}
                @include('employee.controls.head_panel')
            {{--end of head panels--}}


            <!-- Main row -->
<div class="row">
    <!-- Left col -->
    <div class="col-md-8">

        <div class="row">

             {{--direct chat starts here--}}
                 @include('employee.controls.direct_chat')
             {{--direct chat ends here--}}
                <!-- /.col -->

             {{--latest members panel starts here--}}
                 @include('employee.controls.latest_members')
             {{--latest members panel starts here--}}
                <!-- /.col -->
        </div>
            <!-- /.row -->

            <!-- quick email widget -->
            {{--Quick email box starts here--}}
                @include('employee.controls.quick_email')
           {{--Qucick email box ends here--}}

    </div>


    <div class="col-md-4">
        <!-- Info Boxes Style 2 -->
            @include('employee.controls.info_boxes')
        <!-- /.info-box -->


        <!-- PRODUCT LIST -->
            @include('employee.controls.recently_added_product')
        <!-- /.box -->
    </div>

</div>





