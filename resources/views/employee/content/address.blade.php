
{{--code for breadcrumb starts here--}}
@include('employee.controls.breadcrumb',['breadcrumbs'=>[['url'=>'home','text'=>'Dashboard'],['url'=>'#','text'=>'Add New']]])
{{--code for breadcrumb ends here--}}
<div class="box box-solid box-primary" style="background-color: #00b1b3">
    <div class="box-header with-border" style="background-color:#00b1b3">
        <h3 class="box-title">Add New</h3>
        <div class="box-tools pull-right">
            <!-- Buttons, labels, and many other things can be placed here! -->
            <!-- Here is a label for example -->

        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body" style="background-color: #F0F8FF">
        {{--form starts here--}}
        <form action="/address" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('employee.forms.address')
        </form>
        {{--/form ends here--}}
    </div><!-- /.box-body -->
</div>