{{--code for breadcrumb starts here--}}
@include('employee.controls.breadcrumb',['breadcrumbs'=>[['url'=>'home','text'=>'Dashboard'],['url'=>'#','text'=>'']]])
{{--code for breadcrumb ends here--}}
<div class="box box-solid box-primary" style="background-color: #00b1b3;overflow-x: auto;" >
    <div class="box-header with-border" style="background-color:#00b1b3">
        <h3 class="box-title">Orders</h3>
        <div class="box-tools pull-right">
            <!-- Buttons, labels, and many other things can be placed here! -->
            <!-- Here is a label for example -->

        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body" style="color: #ffffff" >
        {{--form starts here--}}
            <table class="table table-bordered table-responsive" border="1" style="    width: 129%!important;
    max-width: 129%!important;">
                <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile Number</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Country</th>
                            <th>Pincode</th>
                            <th>Frame</th>
                            <th>Status</th>
                            <th>Product</th>
                            <th>Amount</th>
                            <th>Ordered on</th>
                            <th>Image</th>
                        </tr>
                </thead>
                <tbody>
                    @forelse($data as $row)
                        <tr>
                            <th>{{ $row->first_name.' '.$row->last_name }}</th>
                            <th>{{ $row->email }}</th>
                            <th>{{ $row->mobile }}</th>
                            <th>{{ $row->address1.' <br/> '.$row->address2  }}</th>
                            <th>{{ $row->city }}</th>
                            <th>{{ $row->state }}</th>
                            <th>{{ $row->country }}</th>
                            <th>{{ $row->zipcode }}</th>
                            <th>{{ $row->frame == 0 ? 'Unframed' : 'Framed' }}</th>
                            <th>{{ $row->status == 0 ? 'Ordered' : 'Delivered' }}</th>
                            <th>{{ $row->product->name }}</th>
                            <th>{{ $row->amount }}</th>
                            <th>{{ $row->created_at }}</th>
                            <th><img src="{{ asset( 'app/'.$row->product->image) }}" class="img-responsive"></th>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="12">
                                No Record Found !
                            </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        {{--/form ends here--}}
    </div><!-- /.box-body -->
</div>