<div class="row">
    <div class="col-md-12">
        @include('layouts.error')
        @include('layouts.success')
        <div>
              {{--product id--}}
            <div class="form-group">
                <label for="product_id">Product Id</label>
                <input class="form-control" type="text" id="product_id" name="product_id" value="{{ $edit->product_id }}">
            </div>
            {{--amount--}}
            <div class="form-group">
                <label for="address">Amount</label>
                <input class="form-control" type="text" id="amount" name="amount" value="{{ $edit->amount}}">
            </div>
             {{--discount--}}
            <div class="form-group">
                <label for="pincode">Discount</label>
                <input class="form-control" type="text" id="discount" name="discount" value="{{ $edit->discount }}">
            </div>
             {{--product_code--}}
            <div class="form-group">
                <label for="state_id">Product Code</label>
                <input class="form-control" type="text" id="product_code" name="product_code" value="{{ $edit->product_code }}">
            </div>
            <br>
            <button type="submit" class="btn btn-primary" style="background-color: #00b1b3">Update</button>
        </div>
    </div>
</div>
