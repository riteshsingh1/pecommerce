<div class="row">
    <div class="col-md-12">
        @include('layouts.error')
        @include('layouts.success')
        <div>
            {{--name--}}
            <div class="form-group">
                <label for="name">Name</label>
                <input class="form-control" type="text" id="name" name="name" value="{{ old('name') }}">
            </div>
        

            {{--image--}}
            <div class="form-group">
            <label for="image">Photo</label>
            <div class="file-field">
                <div class="form-control">
                    <input type="file" id="image" name="image" value="{{ old('image') }}">
                </div>
            </div>
            </div>

            {{--description--}}
            <div class="form-group">
                <label for="description">Description</label>
                <input class="form-control" type="text" id="description" name="description" value="{{ old('description') }}">
            </div>

            {{--category_id dropdown--}}
            <div class="form-group">
                <label for="category_id">Category Id</label>
                    <select id="category_id" class="form-control" name="category_id">
                        <option value="">Please Select Category Id</option>
                            @foreach($designations as $row)
                                 <option value="{{ $row->id }}">{{ $row->name }}</option>
                            @endforeach
                    </select>
            </div>
            {{--category_id ends here--}}

            <br>
            <button type="submit" class="btn btn-primary" style="background-color: #00b1b3">Add Sub Category</button>
        </div>
    </div>
</div>
