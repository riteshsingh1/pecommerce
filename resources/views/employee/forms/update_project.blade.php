<div class="row">
    <div class="col-md-12">
        @include('layouts.error')
        @include('layouts.success')
        <div>
            {{--payment name--}}
            <div class="form-group">
                <label for="name">Payment Name</label>
                <input class="form-control" type="text" id="name" name="name" value="{{ $edit->name }}" >
            </div>

            <br>

            {{--button--}}
            <button type="submit" class="btn btn-primary" style="background-color: #00b1b3">Update</button>
        </div>
    </div>
</div>
