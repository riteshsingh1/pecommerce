<div class="row">
    <div class="col-md-12">
        @include('layouts.error')
        @include('layouts.success')
        <div>
            <input class="form-control" type="text" name="search" id="search" onkeyup="myFunction()" placeholder="Search categories by Name">
            <br>
            {{--employees list code starts here--}}
            <div class="container col-md-12">
                <div style="overflow-x:auto;">
                    <table class="table table-striped col-md-12" id="myTable">
                        <thead>
                            <tr>
                                <th class="warning">ID</th>
                                <th class="warning">Name</th>
                                <th class="warning">Category Id</th>
                                <th class="active">Sub Category Id</th>
                                <th class="warning">Description</th>
                                <th class="warning">Author</th>
                                <th class="warning">Width</th>
                                <th class="warning">Height</th>
                                <th class="warning">Image</th>
                                <th class="warning">Product Id</th>
                                <th class="warning">Amount</th>
                                <th class="warning">Discount</th>
                                <th class="warning">Product Code</th>
                                <th class="warning">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($allusers as $usr)
                            <tr class="info">
                                <td>{{ $usr->id }}</td>
                                <td>{{ $usr->name }}</td>
                                <td>{{ $usr->category_id }}</td>
                                <td>{{ $usr->sub_category_id }}</td>
                                <td>{{ $usr->description }}</td>
                                <td>{{ $usr->author }}</td>
                                <td>{{ $usr->width }}</td>
                                <td>{{ $usr->height }}</td>
                                <td>{{ $usr->image }}</td>
                                <td>{{ $usr->detail->product_id }}</td>
                                <td>{{ $usr->detail->amount }}</td>
                                <td>{{ $usr->detail->discount }}</td>
                                <td>{{ $usr->detail->product_code }}</td>
                                <td><a href="{{ url('/update_product',$usr->id) }}" class="fa fa-edit"></a>
                                    <a href="{{ URL::to('/delete/'.$usr->id) }}" class="fa fa-trash-o"></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            {{--employees list code ends here--}}
        </div>
    </div>
</div>

<script>
    function myFunction() {
        // Declare variables
        var input, filter, table, tr, td, i;
        input = document.getElementById("search");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>


