<div class="row">
    <div class="col-md-12">
        @include('layouts.error')
        @include('layouts.success')
        <div>
            {{--name--}}
            <div class="form-group">
                <label for="name">Name</label>
                <input class="form-control" type="text" id="name" name="name" value="{{ $edit->name }}" >
            </div>
            {{--description--}}
            <div class="form-group">
                <label for="description">Description</label>
                <input class="form-control" type="text" id="description" name="description" value="{{ $edit->description }}" >
            </div>
            {{--status--}}
            <div class="form-group">
                <label for="status">Status</label>
                <input class="form-control" type="text" id="status" name="status" value="{{ $edit->status }}" >
            </div>
           

            {{--image--}}
            <div class ="form-group">
                <label for="image">Image</label>
                <div class="file-field">
                    <div class="form-control">
                        <input type="file" id="image" name="image" >
                    </div>
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-primary" style="background-color: #00b1b3">Update</button>
        </div>
    </div>
</div>
