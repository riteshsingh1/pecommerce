<div class="row">
    <div class="col-md-12">
        @include('layouts.error')
        @include('layouts.success')
        <div>
            {{--name--}}
            <div class="form-group">
                <label for="name">Name</label>
                <input class="form-control" type="text" id="name" name="name" value="{{ $edit->name}}">
            </div>
            {{--category id--}}
            <div class="form-group">
                <label for="category_id">Category Id</label>
                <input class="form-control" type="text" id="category_id" name="category_id" value="{{ $edit->category_id }}">
            </div>
             {{--sub category id--}}
            <div class="form-group">
                <label for="sub_category_id">Sub Category Id</label>
                <input class="form-control" type="text" id="sub_category_id" name="sub_category_id" value="{{ $edit->sub_category_id }}">
            </div>
             {{--description--}}
            <div class="form-group">
                <label for="description">Description</label>
                <input class="form-control" type="text" id="description" name="description" value="{{ $edit->description }}">
            </div>
             {{--author--}}
            <div class="form-group">
                <label for="author">Author</label>
                <input class="form-control" type="text" id="author" name="author" value="{{ $edit->author }}">
            </div>
             {{--width--}}
            <div class="form-group">
                <label for="width">Width</label>
                <input class="form-control" type="text" id="width" name="width" value="{{ $edit->width }}">
            </div>
             {{--height--}}
            <div class="form-group">
                <label for="height">Height</label>
                <input class="form-control" type="text" id="height" name="height" value="{{ $edit->height }}">
            </div>

            {{--image--}}
            <div class="form-group">
            <label for="image">Photo</label>
            <div class="file-field">
                <div class="form-control">
                    <input type="file" id="image" name="image" value="{{ $edit->image }}">
                </div>
            </div>
            </div>
            {{--product id--}}
            <div class="form-group">
                <label for="product_id">Product Id</label>
                <input class="form-control" type="text" id="product_id" name="product_id" value="{{ $edit->product_id }}">
            </div>
            {{--amount--}}
            <div class="form-group">
                <label for="address">Amount</label>
                <input class="form-control" type="text" id="amount" name="amount" value="{{ $edit->amount}}">
            </div>
            {{--discount--}}
            <div class="form-group">
                <label for="pincode">Discount</label>
                <input class="form-control" type="text" id="discount" name="discount" value="{{ $edit->discount }}">
            </div>
            {{--product_code--}}
            <div class="form-group">
                <label for="state_id">Product Code</label>
                <input class="form-control" type="text" id="product_code" name="product_code" value="{{ $edit->product_code }}">
            </div>
            <br>
            <button type="submit" class="btn btn-primary" style="background-color: #00b1b3">Update</button>
        </div>
    </div>
</div>
