<div class="row">
    <div class="col-md-12">
        @include('layouts.error')
        @include('layouts.success')
        <div>
            {{--user id--}}
            <div class="form-group">
                <label for="user_id">User Id</label>
                <input class="form-control" type="text" id="user_id" name="user_id" value="{{ $edit->user_id }}" >
            </div>
           
            {{--address--}}
            <div class="form-group">
                <label for="address">Address</label>
                <input class="form-control" type="text" id="address" name="address" value="{{ $edit->address }}" >
            </div>

             {{--pincode--}}
            <div class="form-group">
                <label for="pincode">Pincode</label>
                <input class="form-control" type="text" id="pincode" name="pincode" value="{{ $edit->pincode }}" >
            </div>

             {{--state id--}}
            <div class="form-group">
                <label for="state_id">State Id</label>
                <input class="form-control" type="text" id="state_id" name="state_id" value="{{ $edit->state_id }}" >
            </div>

             {{--district id--}}
            <div class="form-group">
                <label for="district_id">District Id</label>
                <input class="form-control" type="text" id="district_id" name="district_id" value="{{ $edit->district_id }}" >
            </div>

             {{--milestone--}}
            <div class="form-group">
                <label for="milestone">Milestone</label>
                <input class="form-control" type="text" id="milestone" name="milestone" value="{{ $edit->milestone }}" >
            </div>

             {{--address type--}}
            <div class="form-group">
                <label for="address_type">Address Type</label>
                <input class="form-control" type="text" id="address_type" name="address_type" value="{{ $edit->address_type }}" >
            </div>
            <br>
            <button type="submit" class="btn btn-primary" style="background-color: #00b1b3">Update</button>
        </div>
    </div>
</div>
