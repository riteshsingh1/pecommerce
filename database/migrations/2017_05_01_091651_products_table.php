<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product',function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->integer('category_id');
            $table->integer('sub_category_id');
            $table->text('description');
            $table->string('author')->nullable();
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->text('image')->nullable();
            $table->timestamps();
        });

        Schema::create('product_detail',function (Blueprint $table){
            $table->increments('id');
            $table->string('product_id');
            $table->string('amount');
            $table->string('discount');
            $table->string('product_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
