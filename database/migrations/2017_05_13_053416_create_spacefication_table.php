<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpaceficationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spacefication',function (Blueprint $table){
            $table->increments('id');
            $table->integer('product_id');
            $table->string('Delivery');
            $table->string('Quality');
            $table->string('Durability');
            $table->string('Return Policy');
            $table->string('Size');
            $table->string('Medium');
            $table->string('Cancellation');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
