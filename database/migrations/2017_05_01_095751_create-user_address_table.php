<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_address',function (Blueprint $table){
            $table->increments('id');
            $table->integer('users_id');
            $table->text('address');
            $table->string('pincode');
            $table->integer('state_id');
            $table->integer('district_id');
            $table->string('milestone');
            $table->integer('address_type')->comments('0:home,1:office');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_address');
    }
}
