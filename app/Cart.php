<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table ='cart';
    protected $fillable=['users_id','product_id'];
    protected $dates=['created_at','updated_at'];

    public function product ()
    {
        return $this->belongsTo(Product::class,'product_id');
    }
}
