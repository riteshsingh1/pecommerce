<?php

namespace App\Http\Controllers;

use App\project;
use Illuminate\Http\Request;

class UpdateController extends Controller
{
    /**
     * function to update project in database
     */
    public function update_pro(Request $request , $id)
    {
        $this->validate($request,[
            'name'=>'required',
        ]);


        $user = project::find($id);
        $user->name = $request->name;
        $user->save();

        return redirect('/projects_list')->with('status','Project Details Updated Successfully');
    }






    /**
     * function to delete project from database
     */

    public function delete_pro( $id )
    {
        project::find($id)->delete();
        return redirect('/projects_list')->with('status','Project Deleted Successfully');
    }

}
