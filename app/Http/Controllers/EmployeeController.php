<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Designation;
use App\Employees;
use App\project;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Status;
use App\Address;
use App\Product;
use App\ProductDetail;

class EmployeeController extends Controller
{

    /**
     * function to create new employee
     */
    public function create()
    {

        return view('employee.add_employees');

    }


    /**
     * function to add new designation
     */
    public function adddesignation()
    {
        $designations = Employees::all();
        return view('employee.add_designation',compact('designations'));
    }

    /**
     * function to add new project
     */

    public function addproject()
    {
        $employees = Employees::all();
        return view('employee.add_project',compact('employees'));
    }

    /**
     * function to see list of all employees
     */

    public function list_employeess()
    {
        $allusers = Employees::all();
        return view('employee.employees_list',compact('allusers'));
    }

    /**
     * function to see list of all designations
     */
    public function designation_list()
    {
        $allusers = Designation::all();
        return view('employee.designation_list', compact('allusers'));
    }

    /**
     * function to see list of all projects
     */
    public function project_list()
    {
        $allusers = project::all();
        return view('employee.projects_list',compact('allusers'));
    }


    /**
     * function to view update employees form
     */

    public function update_employes($id)
    {
    
        $edit = Employees::findorFail($id);
        return view('employee.update_employee',compact('edit'));
    }

    /**
     * function to view update project form
     */

    public function media()
    {
        return view('vui.media');
    }

    /**
     * function to view update project form
     */

    public function contact()
    {
        return view('vui.contact');
    }

    /**
     * function to view update project form
     */

    public function forget()
    {
        return view('vui.forget');
    }

    public function charity()
    {
        return view('vui.charity');
    }

    public function myaccount()
    {
        return view('vui.myaccount');
    }

    public function myorder()
    {
        if(Auth::guest()){
            return redirect('/login');
        }
      //  $orders = Bill::where(['user_id'=>Auth::user()->id])->get();
        $orders= DB::table('bill')
            ->join('product', 'product.id', '=', 'bill.product_id')
            ->join('product_detail', 'product_detail.product_id', '=', 'product.id')
            ->select('product.*', 'product_detail.*','product.id as productid')
            ->where(['bill.user_id'=>Auth::user()->id])
            ->get();
        return view('vui.myorder',compact('orders'));
    }

    public function place2()
    {
        return view('vui.place2');
    }

    public function paymentoption()
    {
        return view('vui.paymentoption');
    }

    /**
     * function to view update project form
     */
    public function update_project($id)
    {
    
        $edit = project::findorFail($id);
        return view('employee.update_project',compact('edit'));
    }


    /**
     * function to view update designation form
     */
    public function update_designation($id)
    {
        $designations = Designation::all();
        $edit = Designation::findorFail($id);
        return view('employee.update_designation',compact('designations','edit'));
    }


    /**
     * function to add employees in database
     */
    public function insert(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|max:100',
            'description'=>'required',
            'status'=>'required',
            'image'=>'required'
        ]);

        if ($request->hasFile('image')) {
          $path = $request->image->store('images');
        }

        Employees::create([
            'name'=>$request->name,
            'description'=>$request->description,
            'status'=>$request->status,
            'image'=>$path
            ]);


        return redirect('/employees_list')->with('status','Category added successfully');
    }


    /**
     * function to update employees in database
     */
    public function update(Request $request , $id)
    {
        $this->validate($request,[
            'name'=>'required|max:100',
            'description'=>'required',
            'status'=>'required',
            'image'=>'required'
        ]);



        $user = Employees::find($id);
        $user->name = $request->name;
        $user->description = $request->description;
        $user->status = $request->status;
        $user->image = $request->image;
        $user->save();

        return redirect('/employees_list')->with('status','Details Updated Successfully');
    }




    /**
     * function to update designation in database
     */
    public function update_desig(Request $request , $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'description'=>'required',
            'image'=>'required',
            'category_id'=>'required'
        ]);


        $user = Designation::find($id);
        $user->name = $request->name;
        $user->image = $request->image;
        $user->description = $request->description;
        $user->category_id = $request->category_id;
        $user->save();

        return redirect('/designation_list')->with('status','Details Updated Successfully');
    }


    /**
     * function to delete employee from database
     */

    public function delete_emp( $id )
    {
        Employees::find($id)->delete();
        return redirect('/employees_list')->with('status','Category Deleted Successfully');
    }

    /**
     * function to delete sub category from database
     */

    public function delete_desig( $id )
    {
        Designation::find($id)->delete();
        return redirect('/designation_list')->with('status','Sub Category Deleted Successfully');
    }




    /**
     * function to add new designation in database
     */

    public function add_designations(Request $request)
    {
         $this->validate($request,[
            'name'=>'required|max:100',
            'description'=>'required',
            'category_id'=>'required',
            'image'=>'required'
        ]);
        $path='';
        if ($request->hasFile('image')) {
          $path = $request->image->store('images');
        }

        Designation::create([
            'name'=>$request->name,
            'description'=>$request->description,
            'category_id'=>$request->category_id,
            'image'=>$path
            ]);
        return redirect('/designation_list')->with('status','Sub category added successfully');
    }





    /**
     * function to add new project in database
     */

    public function add_project(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|max:100',
            
        ]);

        project::create([
            'name'=>$request->name,
        ]);

        return redirect('/projects_list')->with('status','Project added successfully');
    }

    /**
     * function to view profile
     */
    public function profile()
    {
        return view('employee.profile');
    }


/**
     * function to view status
     */
    public function show_status()
    {
        return view('employee.status');
    }

    /**
     * function to add new status in database
     */

    public function add_status(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|max:100',
            
        ]);

        Status::create([
            'name'=>$request->name,
        ]);

        return redirect('/status_list')->with('status','status added successfully');
    }

    /**
     * function to see list of all status
     */
    public function status_list()
    {
        $allusers = Status::all();
        return view('employee.status_list',compact('allusers'));
    }


/**
     * function to view update project form
     */
    public function update_stat($id)
    {
    
        $edit = Status::findorFail($id);
        return view('employee.update_status',compact('edit'));
    }

    /**
     * function to update status in database
     */
    public function update_status(Request $request , $id)
    {
        $this->validate($request,[
            'name'=>'required',
        ]);


        $user = Status::find($id);
        $user->name = $request->name;
        $user->save();

        return redirect('/status_list')->with('status','Status Details Updated Successfully');
    }


/**
     * function to delete status from database
     */

    public function delete_status( $id )
    {
        Status::find($id)->delete();
        return redirect('/status_list')->with('status','Status Deleted Successfully');
    }



// Address section

/**
     * function to view address
     */
    public function show_address()
    {
        return view('employee.address');
    }

    /**
     * function to add new status in database
     */

    public function add_address(Request $request)
    {
        $this->validate($request,[
            'user_id'=>'required',
            'address'=>'required',
            'pincode'=>'required',
            'state_id'=>'required',
            'district_id'=>'required',
            'milestone'=>'required',
            'address_type'=>'required',
            
        ]);

        Address::create([
            'user_id'=>$request->user_id,
            'address'=>$request->address,
            'pincode'=>$request->pincode,
            'state_id'=>$request->state_id,
            'district_id'=>$request->district_id,
            'milestone'=>$request->milestone,
            'address_type'=>$request->address_type,
        ]);

        return redirect('/address_list')->with('status','address added successfully');
    }

    /**
     * function to see list of all status
     */
    public function address_list()
    {
        $allusers = Address::all();
        return view('employee.list_address',compact('allusers'));
    }


/**
     * function to view update project form
     */
    public function update_add($id)
    {
    
        $edit = Address::findorFail($id);
        return view('employee.update_address',compact('edit'));
    }

    /**
     * function to update status in database
     */
    public function update_address(Request $request , $id)
    {
        $this->validate($request,[
            'user_id'=>'required',
            'address'=>'required',
            'pincode'=>'required',
            'state_id'=>'required',
            'district_id'=>'required',
            'milestone'=>'required',
            'address_type'=>'required',
        ]);


        $user = Address::find($id);
        $user->user_id = $request->user_id;
        $user->address = $request->address;
        $user->pincode = $request->pincode;
        $user->state_id = $request->state_id;
        $user->district_id = $request->district_id;
        $user->milestone = $request->milestone;
        $user->address_type = $request->address_type;
        $user->save();

        return redirect('/address_list')->with('status','Address Details Updated Successfully');
    }


/**
     * function to delete status from database
     */

    public function delete_address( $id )
    {
        Address::find($id)->delete();
        return redirect('/address_list')->with('status','Address Deleted Successfully');
    }



// product section

    /**
     * function to view address
     */
    public function show_product()
    {
        return view('employee.product');
    }

    /**
     * function to add new status in database
     */

    public function add_product(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'category_id'=>'required',
            'sub_category_id'=>'required',
            'description'=>'required',
            'author'=>'required',
            'width'=>'required',
            'height'=>'required',
            'image'=>'required',
            'product_id'=>'required',
            'amount'=>'required',
            'discount'=>'required',
            'product_code'=>'required'
            
        ]);

if ($request->hasFile('image')) {
          $path = $request->image->store('images');
        }

        $product=Product::create([
            'name'=>$request->name,
            'category_id'=>$request->category_id,
            'sub_category_id'=>$request->sub_category_id,
            'description'=>$request->description,
            'author'=>$request->author,
            'width'=>$request->width,
            'height'=>$request->height,
            'image'=>$path,

        ]);

        ProductDetail::create([
            'product_id'=>$product->id,
            'amount'=>$request->amount,
            'discount'=>$request->discount,
            'product_code'=>$request->product_code,
        ]);

        return redirect('/product_list')->with('status','product added successfully');
    }

    /**
     * function to see list of all status
     */
    public function product_list()
    {
        $allusers = Product::all();
        return view('employee.list_product',compact('allusers'));
    }


/**
     * function to view update project form
     */
    public function update_prod($id)
    {
    
        $edit = Product::findorFail($id);
        $editt = ProductDetail::findorFail($id);
        return view('employee.update_product',compact('edit','editt'));
    }

    /**
     * function to update status in database
     */
    public function update_product(Request $request , $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'category_id'=>'required',
            'sub_category_id'=>'required',
            'description'=>'required',
            'author'=>'required',
            'width'=>'required',
            'height'=>'required',
            'image'=>'required',
            'product_id'=>'required',
            'amount'=>'required',
            'discount'=>'required',
            'product_code'=>'required'
        ]);


        $user = Product::find($id);
        $user->name = $request->name;
        $user->category_id = $request->category_id;
        $user->sub_category_id = $request->sub_category_id;
        $user->description = $request->description;
        $user->author = $request->author;
        $user->width = $request->width;
        $user->height = $request->height;
        $user->image = $request->image;
        $user->save();

        $users = ProductDetail::find($id);
        $users = ProductDetail::find($id);
        $users->product_id = $request->product_id;
        $users->amount = $request->amount;
        $users->discount = $request->discount;
        $users->product_code = $request->product_code;

        $users->save();

        return redirect('/product_list')->with('status','Product Details Updated Successfully');
    }


/**
     * function to delete status from database
     */

    public function delete_Product( $id )
    {
        Product::find($id)->delete();
        ProductDetail::find($id)->delete();
        return redirect('/product_list')->with('status','Product Deleted Successfully');
    }







    // product detail section

    /**
     * function to view address
     */
    public function show_product_detail()
    {
        return view('employee.product_detail');
    }

    /**
     * function to add new status in database
     */

    public function add_product_detail(Request $request)
    {
        $this->validate($request,[
            'product_id'=>'required',
            'amount'=>'required',
            'discount'=>'required',
            'product_code'=>'required',
            
            
        ]);



        ProductDetail::create([
            'product_id'=>$request->product_id,
            'amount'=>$request->amount,
            'discount'=>$request->discount,
            'product_code'=>$request->product_code,
           
        ]);

        return redirect('/product_detail_list')->with('status','product detail added successfully');
    }

    /**
     * function to see list of all status
     */
    public function product_detail_list()
    {
        $allusers = ProductDetail::all();
        return view('employee.list_product_detail',compact('allusers'));
    }


/**
     * function to view update project form
     */
    public function update_prod_detail($id)
    {
    
        $edit = ProductDetail::findorFail($id);
        return view('employee.update_product_detail',compact('edit'));
    }

    /**
     * function to update status in database
     */
    public function update_product_detail(Request $request , $id)
    {
        $this->validate($request,[
           'product_id'=>'required',
            'amount'=>'required',
            'discount'=>'required',
            'product_code'=>'required',
        ]);


        $user = ProductDetail::find($id);
        $user->product_id = $request->product_id;
        $user->amount = $request->amount;
        $user->discount = $request->discount;
        $user->product_code = $request->product_code;
       
        $user->save();

        return redirect('/product_detail_list')->with('status','Product Details Updated Successfully');
    }


/**
     * function to delete status from database
     */

    public function delete_Product_detail( $id )
    {
        ProductDetail::find($id)->delete();
        return redirect('/product_detail_list')->with('status','Product Deleted Successfully');
    }

}
