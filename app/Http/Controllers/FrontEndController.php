<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Cart;
use App\Invoice;
use App\Mail\ResetPasswordAtStylishDecor;
use App\product;
use App\ProductDetail;
use App\Specification;
use App\UserAddress;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Razorpay\Api\Api;
use Softon\Indipay\Indipay;

class FrontEndController extends Controller
{

    function __construct()
    {
        session_start();
    }

    public function index ()
    {
        return view('vui.index');
    }

    public function show_images()
    {
        $product = product::where(['category_id'=>'1'])->get();
        $product_price = ProductDetail::where(['product_id'=>'1'])->get();
        $main_slider = '';
        $about_us = '';
        $showcase = '';
        $portfolio = '';
        $latest_design = '';

        foreach ($product as $row){
            if($row->sub_category_id == '1'){
                $main_slider[] = $row;
            }
            if($row->sub_category_id == '2'){
                $about_us[] = $row;
            }
            if($row->sub_category_id == '3'){
                $showcase[] = $row;
            }
            if($row->sub_category_id == '4'){
                $portfolio[] = $row;
            }
            if($row->sub_category_id == '5'){
                $latest_design[] = $row;
            }
        }
//        echo $count = Auth::user()->cart->count();exit;
        return view('vui.index',compact('product','main_slider','about_us','showcase','portfolio','latest_design','product_price'));
    }

    public function show_single($id)
    {

        $count = 0;
        $product = Product::find($id);

        $demo = $product->detail->amount;

        return view('vui.single',compact('count','product'));
    }
     protected function razorPay($receipt,$total_amt){
//            $api_key='rzp_test_GgrXT1PezuxGFr';
//            $api_secret='ZNWKOmRrUNpG0vm5NesGUW9o';
            $api_key='rzp_test_XWnWKYbN1t0d89';
            $api_secret='OMRTsutyGAs8VMaiSDI99Fl9';
            $api = new Api($api_key, $api_secret);
            $orderData = [
                'receipt'         => $receipt,
                'amount'          => $total_amt*100, // 2000 rupees in paise
                'currency'        => 'INR',
                'payment_capture' => 1 // auto capture
            ];
            $razorpayOrder = $api->order->create($orderData);
         //   Helper::prints($razorpayOrder);
            $razorpayOrderId = $razorpayOrder['id'];
            $_SESSION['razorpay_order_id'] = $razorpayOrderId;
            $displayAmount = $amount = $orderData['amount'];
            $api_array=['order_id'=>$razorpayOrderId,'total'=>$displayAmount];

            return $api_array;
        }
     public function purchase (Request $request)
    {
//        $api_key='rzp_test_GgrXT1PezuxGFr';
//        $api_secret='ZNWKOmRrUNpG0vm5NesGUW9o';
           $api_key='rzp_test_XWnWKYbN1t0d89';
            $api_secret='OMRTsutyGAs8VMaiSDI99Fl9';
        $success = true;

        $error = "Payment Failed";
//
       if (empty($_POST['razorpay_payment_id']) === false)
       {
           $api = new Api($api_key, $api_secret);

       }
//         $id = Invoice::all()->count();
//         $id= $id+1;
//        Invoice::create([
//            'order_id'=>$id,
//            'transaction_id'=>$id,
//            'product_id'=>202,
//            'users_id'=>Auth::user()->id,
//            'amount'=>Auth
//        ]);

        return redirect()->back()->with('status','Your Purchase was successful');
    }


    public  function aboutus()
    {
        return view ('vui.about-us');
    }
    public function charity()
    {
        return view('vui.charity');
    }

    public function palce2()
    {
        return view('vui.place2');
    }

    public function paymentoption()
    {
        return view('vui.paymentoption');
    }
//    function to add items into cart
    public function add_cart($id)
    {
//       // echo $id.'<br/>';
        $cc=Cart::where(['product_id'=>$id,'users_id'=>Auth::user()->id])->get()->count();

        if($cc == 0){
            Cart::create([
                'users_id'=>Auth::user()->id,
                'product_id'=>$id
            ]);

        }else{
            return redirect()->back()->with('status','Item Already Added to Cart');
        }
        return redirect()->back()->with('status','Added to Cart');

    }


//    function to show images in talk town category in menu
    public function show_talk_town()
    {
        $product = Product::where(['category_id'=>'2'])->get();
        $product_price = ProductDetail::where(['product_id'=>'2'])->get();
        $talk_town = '';


        foreach ($product as $row){
            if($row->sub_category_id == '1'){
                $talk_town[] = $row;

            }

        }
        return view('vui.products',compact('product','talk_town','product_price'));
    }


//    function to show images in signature category in menu
    public function signature()
    {
        $product = product::where(['category_id'=>'2'])->get();
        $product_price = ProductDetail::where(['product_id'=>'2'])->get();
        $talk_town = '';


        foreach ($product as $row){
            if($row->sub_category_id == '2'){
                $talk_town[] = $row;

            }


        }
        return view('vui.products',compact('product','talk_town','product_price'));
    }


    //    function to show images in luxury category in menu
    public function luxury()
    {
        $product = product::where(['category_id'=>'2'])->get();
        $product_price = ProductDetail::where(['product_id'=>'2'])->get();
        $talk_town = '';


        foreach ($product as $row){
            if($row->sub_category_id == '3'){
                $talk_town[] = $row;

            }


        }
        return view('vui.products',compact('product','talk_town','product_price'));
    }


    //    function to show images in elite category in menu
    public function elite()
    {
        $product = product::where(['category_id'=>'2'])->get();
        $product_price = ProductDetail::where(['product_id'=>'2'])->get();
//        dd($product_price);exit;
        $talk_town = '';



        foreach ($product as $row){
            if($row->sub_category_id == '4'){
                $talk_town[] = $row;

            }
        }
        return view('vui.products',compact('product','talk_town','product_price'));
    }



    //    function to show the items added into cart
    public function show_cart()
    {
        if(Auth::guest()){
            return redirect('/login');
        }

        $cart = Cart::where(['users_id'=>Auth::user()->id])->with('product')->get();
       // $cart = Cart::where(['users_id'=>'1'])->with('product')->get();
       // dd($cart);
        $count = Cart::where(['users_id'=>Auth::user()->id])->get()->count();
      $cart= DB::table('cart')
            ->join('product', 'product.id', '=', 'cart.product_id')
            ->join('product_detail', 'product_detail.product_id', '=', 'product.id')
            ->select('product.*', 'product_detail.*','cart.id as cartid')
            ->where(['users_id'=>Auth::user()->id])
            ->get();
     // dd($cart);
     // dd($cart);
        $count=$count-1;
        return view('vui.cart',compact('cart','count'));
    }



    public function place()
    {

        if(Auth::guest()){
            return redirect('/login');
        }

        $cart = Cart::where(['users_id'=>Auth::user()->id])->with('product')->get();
        // $cart = Cart::where(['users_id'=>'1'])->with('product')->get();
        // dd($cart);
        $count = Cart::where(['users_id'=>Auth::user()->id])->get()->count();

        $cart= DB::table('cart')
            ->join('product', 'product.id', '=', 'cart.product_id')
            ->join('product_detail', 'product_detail.product_id', '=', 'product.id')
            ->select('product.*', 'product_detail.*','cart.id as cartid')
            ->where(['users_id'=>Auth::user()->id])
            ->get();

        return view('vui.place',compact('cart','count'));
    }

    public function place2()
    {
        // Merchant key here as provided by Payu
        $MERCHANT_KEY = "G2mWTBnv";
// Merchant Salt as provided by Payu
        $SALT = "zi9JiULlzz";

// End point - change to https://secure.payu.in for LIVE mode
        $PAYU_BASE_URL = "https://test.payu.in";

        $action = '';

        $posted = array();
        if(!empty($_POST)) {
            //print_r($_POST);
            foreach($_POST as $key => $value) {
                $posted[$key] = $value;

            }
        }

        $formError = 0;

        if(empty($posted['txnid'])) {
            // Generate random transaction id
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        } else {
            $txnid = $posted['txnid'];
        }
        $hash = '';
// Hash Sequence
        $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        if(empty($posted['hash'])) {
            if(
                $txnid
//                empty($posted['key'])
//                || empty($posted['txnid'])
//                || empty($posted['amount'])
//                || empty($posted['firstname'])
//                || empty($posted['email'])
//                || empty($posted['phone'])
//                || empty($posted['productinfo'])
//                || empty($posted['surl'])
//                || empty($posted['furl'])
//                || empty($posted['service_provider'])
            ) {

                $formError = 1;
            } else {
                //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
                $hashVarsSeq = explode('|', $hashSequence);
                $hash_string = '';
                foreach($hashVarsSeq as $hash_var) {
                    $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                    $hash_string .= '|';
                }

                $hash_string .= $SALT;


                $hash = strtolower(hash('sha512', $hash_string));
                $action = $PAYU_BASE_URL . '/_payment';
                dd($hash);
            }
        } elseif(!empty($posted['hash'])) {
            $hash = $posted['hash'];
            $action = $PAYU_BASE_URL . '/_payment';
        }
        dd($txnid);

    }
    public  function save(Request $request){

        // Merchant key here as provided by Payu
        $MERCHANT_KEY = "G2mWTBnv";
// Merchant Salt as provided by Payu
        $SALT = "zi9JiULlzz";

// End point - change to https://secure.payu.in for LIVE mode
        $PAYU_BASE_URL = "https://test.payu.in";

        $action = '';

        $posted = array();
        if(!empty($request->all())) {
            //print_r($_POST);
            foreach($request->all() as $key => $value) {
                $posted[$key] = $value;

            }
        }

        $formError = 0;

        if(empty($request->txnid)) {
            // Generate random transaction id
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        } else {
            $txnid = $request->txnid;
        }
        $hash = '';
// Hash Sequence
        $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        if(empty($request->hash)) {
            if(
                empty($posted['key'])
//                || empty($posted['txnid'])
//                || empty($posted['amount'])
//                || empty($posted['firstname'])
//                || empty($posted['email'])
//                || empty($posted['phone'])
//                || empty($posted['productinfo'])
//                || empty($posted['surl'])
//                || empty($posted['furl'])
//                || empty($posted['service_provider'])
            ) {

                $formError = 1;
            } else {
                //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
                $hashVarsSeq = explode('|', $hashSequence);
                $hash_string = '';
                foreach($hashVarsSeq as $hash_var) {
                    $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                    $hash_string .= '|';
                }

                $hash_string .= $SALT;


                $hash = strtolower(hash('sha512', $hash_string));
                $action = $PAYU_BASE_URL . '/_payment';
                dd($hash);
            }
        } elseif(!empty($posted['hash'])) {
            $hash = $posted['hash'];
            $action = $PAYU_BASE_URL . '/_payment';
        }
    }


    public function checkout(Request $request)
    {
        UserAddress::create([
            'users_id'=>Auth::user()->id,
            'address'=>$request->address,
            'pincode'=>$request->pincode,
            'state_id'=>$request->state_id,
            'district_id'=>$request->district_id,
            'milestone'=>$request->landmark,
            'address_type'=>1
        ]);
        $total = 0;
        $cart =Cart::where(['users_id'=>Auth::user()->id])->get();
        foreach ($cart as $row){
            $total+=$row->product->detail->amount;
        }
        $total=$total+100;
        return view('vui.payment',compact($total));
    }
    public function deletecart ($id)
    {

        Cart::find($id)->delete();
        echo 'success';
    }
    public function mi($cat,$sub)
    {
        $q = Product::where(['category_id'=>$cat,'sub_category_id'=>$sub])->get();
        foreach ($q as $row){
            Specification::create([
                'product_id'=>$row->id,
                'Delivery'=>'Delivery :Delivered within 15 to 20 working days when ordered as unframed and 20 to 25 days when ordered as framed ',
                'Quality'=>' Superior quality stretch canvas',
                'Durability'=>'More than 50 years with proper care and maintenance',
                'Return_Policy'=>'7 days after receiving the painting if ordered as unframed . Not applicable if ordered as framed',
                'Medium'=>'Acrylic oil mix on canvas',
                'Size'=>'0',
                'Cancellation'=>'Cannot cancel once the order is placed',
            ]);
        }
    }

    public function success()
    {

        return view('vui.success');
    }

    public function reset(Request $request)
    {
        Mail::to($request->email)->send(new ResetPasswordAtStylishDecor());

       return redirect()->back()->with('status','Mail Sent Successfully');
    }

    public function failure()
    {
        return view('vui.failure');
    }
    public function prepare ()
    {
        $total = $_SESSION['total'] ?? 00;
        if(Auth::guest()){
            return redirect('/login');
        }

        $cart = Cart::where(['users_id'=>Auth::user()->id])->with('product')->get();
        // $cart = Cart::where(['users_id'=>'1'])->with('product')->get();
        // dd($cart);
        $count = Cart::where(['users_id'=>Auth::user()->id])->get()->count();

        $cart= DB::table('cart')
            ->join('product', 'product.id', '=', 'cart.product_id')
            ->join('product_detail', 'product_detail.product_id', '=', 'product.id')
            ->select('product.*', 'product_detail.*','cart.id as cartid')
            ->where(['users_id'=>Auth::user()->id])
            ->get();

        return view('vui.place',compact('cart','count'));

//        dd($total);
//        return view('vui.place',compact($total));
    }
    public function go (Request $request)
    {
       // dd($request->all());

        $parameters = [

            'tid' => str_random(10),
            'order_id' => mt_rand(8,12),
            'amount' => $request->amount,
            'firstname' => $request->firstname,
            'email' => $request->email,
            'phone' => $request->phone,
            'productinfo' =>$request->productinfo,
            'address1'=>$request->address1,
            'address2'=>$request->address2,
            'city'=>$request->city,
            'state'=>$request->state,
            'zipcode'=>$request->zipcode

        ];

        $order = \Softon\Indipay\Facades\Indipay::gateway('PayUMoney')->prepare($parameters);;
        return \Softon\Indipay\Facades\Indipay::process($order);

    }
    public function proceedcheckout (Request $request)
    {
        $_SESSION['total']=$request->amount;

        return redirect('/checkout');

    }

//    checkout cash on delivery
    public function cashondelivery(){
        if(Auth::guest()){
            return redirect('/login');
        }

        $cart = Cart::where(['users_id'=>Auth::user()->id])->with('product')->get();
        // $cart = Cart::where(['users_id'=>'1'])->with('product')->get();
        // dd($cart);
        $count = Cart::where(['users_id'=>Auth::user()->id])->get()->count();

        $cart= DB::table('cart')
            ->join('product', 'product.id', '=', 'cart.product_id')
            ->join('product_detail', 'product_detail.product_id', '=', 'product.id')
            ->select('product.*', 'product_detail.*','cart.id as cartid')
            ->where(['users_id'=>Auth::user()->id])
            ->get();

        return view('vui.cashondelivery',compact('cart','count'));

    }
    /**
     * Save Cash on delivery method
     */
    //    checkout cash on delivery
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cashondeliverySave(Request $request){
        foreach ($request->cart_items as $row){
            Bill::create([
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'email'=>$request->email,
                'mobile'=>$request->mobile,
                'address1'=>$request->address1,
                'address2'=>$request->address2,
                'city'=>$request->city,
                'state'=>$request->state,
                'country'=>$request->country,
                'zipcode'=>$request->zipcode,
                'frame'=>$request->frame,
                'status'=>1,// order placed but not dispatched
                'user_id'=>Auth::user()->id,
                'product_id'=>$row,
                'amount'=>$request->amount
            ]);
        }
        foreach ($request->cart_items as $rows){
//            $product=Product::where('id','=',$rows)->get()->first();
//            $product->status=0;
//            $product->save();
            DB::update(DB::raw("update `product` set status='0' WHERE `id`='$rows'"));
        }

        Cart::where(['users_id'=>Auth::user()->id])->delete();

        return redirect('/')->with('alerts','Your order has been placed successfully');
    }
    public function ordersmgt ()
    {
        $data = Bill::all();

        return view('employee.orders',compact('data'));
    }
}
