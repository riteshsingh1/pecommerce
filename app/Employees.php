<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $table='category';
    protected $fillable=['name','image','description','status'];
    protected $dates=['created_at','updated_at'];
}
