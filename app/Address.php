<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table='user_address';
    protected $fillable=['user_id','address','pincode','state_id','district_id','milestone','address_type'];
    protected $dates = ['created_at','updated_at'];}
