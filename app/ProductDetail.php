<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    protected $table='product_detail';
    protected $fillable=['product_id','amount','discount','product_code'];
    protected $dates = ['created_at','updated_at'];
}
