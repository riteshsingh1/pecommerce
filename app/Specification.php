<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specification extends Model
{
    protected $table='spacefication';
    public $fillable=['product_id','Delivery','Quality','Durability','Return_Policy','Medium','Size','Cancellation'];
}
