<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class project extends Model
{
    protected $table='payment_mode';
    protected $fillable=['name'];
    protected $dates=['created_at','updated_at'];
}


