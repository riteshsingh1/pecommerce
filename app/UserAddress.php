<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $table='user_address';
    protected $fillable=['users_id','address','pincode','state_id','district_id','milestone','address_type'];
    public $timestamps = false;
}
