<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='product';
    protected $fillable=['name','status','category_id','sub_category_id','description','author','width','height','image'];
    protected $dates = ['created_at','updated_at'];

    public function detail ()
    {
        return $this->hasOne('App\\ProductDetail');
    }
    public function spec ()
    {
        return $this->hasOne('App\\Specification');
    }
}
