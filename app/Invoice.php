<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table='invoice';
    protected $fillable=['order_id','transaction_id','product_id','users_id','amount','tax','address','pincode'];
}
