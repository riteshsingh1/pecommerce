<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $table='sub_category';
    protected $fillable=['name','description','image','category_id'];
    protected $dates=['created_at','updated_at'];

}
