<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $table='bill';
    protected $fillable=['product_id','amount','first_name','last_name','email','mobile','address1','address2','city','state','country','zipcode','frame','status','user_id'];

    public function product() {
        return $this->belongsTo(Product::class,'product_id');
    }
}
