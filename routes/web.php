<?php
/* route for login ends here */

Auth::routes();
Route::get('/', 'FrontEndController@show_images');
Route::get('single/{id}','FrontEndController@show_single');
Route::get('/home', 'HomeController@index');
Route::get('about-us', 'FrontEndController@aboutus');
Route::get('cart/{id}','FrontEndController@add_cart');
Route::get('cart','FrontEndController@show_cart');
Route::get('talk_town','FrontEndController@show_talk_town');
Route::get('signature','FrontEndController@signature');
Route::get('luxury','FrontEndController@luxury');
Route::get('elite','FrontEndController@elite');
Route::get('place','FrontEndController@place');
Route::get('place2','FrontEndController@place2');
Route::get('paymentoption','FrontEndController@paymentoption');
Route::get('media','FrontEndController@media');
Route::get('forget','FrontEndController@forget');
Route::get('charity','FrontEndController@charity');
Route::post('place2','FrontEndController@place2');
Route::get('contact','FrontEndController@contact');
Route::get('delete-cart/{id}','FrontEndController@deletecart');
Route::post('proceed-checkout','FrontEndController@proceedcheckout');
Route::get('checkout','FrontEndController@prepare');
Route::post('checkout','FrontEndController@go');
//Route::post('reset','FrontEndController@reset');
//Route::get('PayUMoney_form','FrontEndController@payumoney');
Route::get('success','FrontEndController@success');
Route::post('success','FrontEndController@success');
Route::post('failure','FrontEndController@failure');
Route::get('failure','FrontEndController@failure');
Route::get('myaccount','FrontEndController@myaccount');
Route::get('myorder','FrontEndController@myorder');
Route::get('mi/{category}/{subcategory}','FrontEndController@mi');

/* route for logout starts here */
Route::get('/logout', 'Auth\LoginController@logout');
/* route for logout ends here */

/* route for create employee starts here */
Route::get('/add_employees', 'EmployeeController@create');
/* route for create employee ends here */
Route::get('/charity','EmployeeController@charity');

Route::get('/place2','EmployeeController@place2');

Route::get('/paymentoption','EmployeeController@paymentoption');
Route::get('/myaccount','EmployeeController@myaccount');

Route::get('/myorder','EmployeeController@myorder');

/**
 * place order
 */
Route::get('place-order','FrontEndController@cashondelivery');
Route::post('place-order','FrontEndController@cashondeliverySave');

/**
 * Place order ends here
 */

/* route for add designation starts here */
Route::get('/add_designation','EmployeeController@adddesignation');
/* route for add designation ends here */

/* route for add project starts here */
Route::get('/add_project','EmployeeController@addproject');
/* route for add project ends here */

/* route for list employees starts here */
Route::get('/employees_list','EmployeeController@list_employeess');
/* route for list employees ends here */

/* route for update employees starts here */
Route::get('/update_employee/{id}','EmployeeController@update_employes');
/* route for update employees ends here */

/* route for delete employees starts here */
Route::get('/delete_employee/{id}','EmployeeController@delete_emp');
/* route for delete employees ends here */

/* route for delete sub category starts here */
Route::get('/delete_designation/{id}','EmployeeController@delete_desig');
/* route for delete sub category ends here */

/* route for list designation starts here */
Route::get('/designation_list','EmployeeController@designation_list');
/* route for list designation ends here */

/* route for list projects starts here */
Route::get('/projects_list','EmployeeController@project_list');
/* route for list projects ends here */

/* route for update employees starts here */
Route::get('/update_designation/{id}','EmployeeController@update_designation');
/* route for update employees ends here */

/* route for update employees starts here */
Route::get('/update_project/{id}','EmployeeController@update_project');
/* route for update employees ends here */

/* route for delete project starts here */
Route::get('/delete/{id}','UpdateController@delete_pro');
/* route for delete project ends here */

/* route for view profile starts here */
Route::get('/profile','EmployeeController@profile');
/* route for view profile ends here */

/* route for view profile starts here */
Route::get('/media','EmployeeController@media');
/* route for view profile ends here */

/* route for view profile starts here */
Route::get('/contact','EmployeeController@contact');
/* route for view profile ends here */

/* route for view profile starts here */
Route::get('/forget','EmployeeController@forget');
/* route for view profile ends here */

/* route for view profile starts here */

/* route for view profile ends here */

// status section
/* route for view profile starts here */
Route::get('/status','EmployeeController@show_status');
/* route for view profile ends here */

/* route for delete project starts here */
Route::get('/delete/{id}','EmployeeController@delete_status');
/* route for delete project ends here *

/* route for update employees starts here */
Route::get('/update_status/{id}','EmployeeController@update_stat');
/* route for update employees ends here */

/* route for list projects starts here */
Route::get('/status_list','EmployeeController@status_list');
/* route for list projects ends here */


// address section

// status section
/* route for view profile starts here */
Route::get('/address','EmployeeController@show_address');
/* route for view profile ends here */

/* route for delete project starts here */
Route::get('/delete/{id}','EmployeeController@delete_address');
/* route for delete project ends here *

/* route for update employees starts here */
Route::get('/update_address/{id}','EmployeeController@update_add');
/* route for update employees ends here */

/* route for list projects starts here */
Route::get('/address_list','EmployeeController@address_list');
/* route for list projects ends here */



// product section

// status section
/* route for view profile starts here */
Route::get('/product','EmployeeController@show_product');
/* route for view profile ends here */

/* route for delete project starts here */
Route::get('/delete/{id}','EmployeeController@delete_product');
/* route for delete project ends here *

/* route for update employees starts here */
Route::get('/update_product/{id}','EmployeeController@update_prod');
/* route for update employees ends here */

/* route for list projects starts here */
Route::get('/product_list','EmployeeController@product_list');
/* route for list projects ends here */




// product detail section

// status section
/* route for view profile starts here */
Route::get('/product_detail','EmployeeController@show_product_detail');
/* route for view profile ends here */

/* route for delete project starts here */
Route::get('/delete/{id}','EmployeeController@delete_product_detail');
/* route for delete project ends here *

/* route for update employees starts here */
Route::get('/update_product_detail/{id}','EmployeeController@update_prod_detail');
/* route for update employees ends here */

/* route for list projects starts here */
Route::get('/product_detail_list','EmployeeController@product_detail_list');
/* route for list projects ends here */




/* route for insert employee into database starts here */
Route::post('/insert','EmployeeController@insert');
/* route for insert employee into database ends here */

/* route for insert designation into database starts here */
Route::post('/add','EmployeeController@add_designations');
/* route for insert designation into database ends here */

/* route for insert project into database starts here */
Route::post('/add_project','EmployeeController@add_project');
/* route for insert project into database ends here */

/* route for update employee information into database starts here */
Route::post('/update_employee/{id}','EmployeeController@update');
/* route for update employee information into database ends here */

/* route for update designation into database starts here */
Route::post('/update_designation/{id}','EmployeeController@update_desig');
/* route for update designation into database ends here */

/* route for update employee information into database starts here */
Route::post('/update/{id}','UpdateController@update_pro');
/* route for update employee information into database ends here */

/* route for attendance into database starts here */
Route::post('/attendance','EmployeeController@attendance');
/* route for attendance into database ends here */


/* route for insert employee into database starts here */
Route::post('/status','EmployeeController@add_status');
/* route for insert employee into database ends here */

/* route for update employee information into database starts here */
Route::post('/update_status/{id}','EmployeeController@update_status');
/* route for update employee information into database ends here */


/* route for insert employee into database starts here */
Route::post('/address','EmployeeController@add_address');
/* route for insert employee into database ends here */

/* route for update employee information into database starts here */
Route::post('/update_address/{id}','EmployeeController@update_address');
/* route for update employee information into database ends here */



/* route for insert employee into database starts here */
Route::post('/product','EmployeeController@add_product');
/* route for insert employee into database ends here */

/* route for update employee information into database starts here */
Route::post('/update_product/{id}','EmployeeController@update_product');
/* route for update employee information into database ends here */



/* route for insert employee into database starts here */
Route::post('/product_detail','EmployeeController@add_product_detail');
/* route for insert employee into database ends here */

/* route for update employee information into database starts here */
Route::post('/update_product_detail/{id}','EmployeeController@update_product_detail');
/* route for update employee information into database ends here */
Route::post('purchase', 'FrontEndController@purchase');

Route::get('all-orders','FrontEndController@ordersmgt');